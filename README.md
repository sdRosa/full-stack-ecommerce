# Ecommerce Full Stack

The Ecommerce is based o FastAPI, Angular, Redis and PostgreSQL includes the following features:

1. Shop by categories
2. Cart
3. Order Summary
5. Checkout
6. Default Addresses
7. Order History

#### Backend Instalation

First we have to create the virtual environment with [virtualenv](https://github.com/pypa/virtualenv) inside the backend folder:

```
pip install virtualenv 
virtualenv env 
```

Activate the virtualenv:

```
source env/scripts/activate
```

And install the required modules:

```
pip install -r requirements.tx
```

Then, create a file `.env` inside `backend/app` that will have all envirioment variables that are required. The following table show all variables:

| Variable | Type |
| ---------------------------- | ------------- |
| DATABASE_URL  | String  |
| ACCESS_TOKEN_EXPIRES_MINUTES  | Integer  |
| DOMAIN_HOST  | String  |
| DOMAIN_PORT  | Integer  |
| REDIS_HOST  | String  |
| REDIS_PORT  | Integer  |
| REDIS_DB  | Integer  |
| BACKEND_CORS_ORIGINS  | Array[String]  |
| SECRET_KEY  | Integer  |
| ALGORITHM  | String  |
| PROJECT_NAME  | String  |}
| API_V1_STR  | String  |

Finally, run the server using `py main.py`.

#### Frontend Instalation

Run `npm install` inside `frontend` folder, then `npm start`.
