from typing import (
    Any, 
    Dict, 
    Generic, 
    List, 
    Optional, 
    Type, 
    TypeVar, 
    Union)
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel

from app.db.db_setup import Base, db

ModelType = TypeVar("ModelType", bound=Base)

SchemaType = TypeVar("SchemaType", bound=BaseModel)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class CRUDBase(Generic[SchemaType, CreateSchemaType, UpdateSchemaType]):
    def __init__(self, model: Type[ModelType]):
        """
        CRUD object with default methods to Create, Read, Update, Delete (CRUD).

        **Parameters**

        * `model`: A SQLAlchemy model class
        * `schema`: A Pydantic model (schema) class
        """
        self.model = model

    async def get(
        self, 
        id: int, 
    ) -> Optional[ModelType]:
        query = self.model.select().where(
            self.model.c.id == id
        )
        return await db.fetch_one(query)

    async def get_by_name(
        self, 
        *, 
        name: str, 
    ) -> Optional[ModelType]:

        query = self.model.select().where(self.model.c.name == name)
        
        return await db.fetch_one(query)

    async def get_multi(
        self, *, 
        skip: int = 0, 
        limit: int = 100, 
        is_active: bool = True,
    ) -> List[ModelType]:
        query = self.model.select().where(
            self.model.c.is_active == is_active
        ).offset(skip).limit(limit)

        obj_list = await db.fetch_all(query)
        return [dict(result) for result in obj_list]
 

    async def create(
        self, 
        *, 
        obj_in: CreateSchemaType, 
    ) -> Optional[ModelType]:
        query = self.model.insert().values(**obj_in.dict())
        return await db.execute(query)


    async def update(
        self,
        *,
        db_obj: Session,
        obj_in: Union[UpdateSchemaType, Dict[str, Any]]
    ) -> ModelType:
        obj_data = jsonable_encoder(db_obj.dict())
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
            
        for field in obj_data:
            if field in update_data:
                setattr(db_obj, field, update_data[field])

        query = self.model.update().where(
            self.model.c.id == db_obj.id
        ).values(**db_obj.dict()).returning(self.model)
        return await db.fetch_one(query=query)


    async def remove(self, *, id: int) -> ModelType:
        obj_del = self.model.delete().where(
            self.model.c.id == id
        )
        return await db.execute(obj_del)
