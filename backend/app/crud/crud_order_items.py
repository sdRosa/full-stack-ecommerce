from typing import List

from app.crud.base import CRUDBase
from app.models.order_item import OrderItem, order_item
from app.schemas.order_item_sch import OrderItemSch, OrderItemCreatePriv, OrderItemUpdate
from app.db.db_setup import db
 

class CRUDOrderItem(CRUDBase[OrderItemSch, OrderItemCreatePriv, OrderItemUpdate]):
    async def create_bulk(
        self, 
        bulk_obj: List[OrderItemCreatePriv]
    ) -> List[OrderItem]:
        if len(bulk_obj) > 1:
            query = self.model.insert().values(bulk_obj).returning(
                self.model.c.id, self.model.c.item_id, self.model.c.quantity
            )
            return await db.fetch_all(query)

        query = self.model.insert().values(**bulk_obj[0]).returning(
                self.model.c.id, self.model.c.item_id, self.model.c.quantity
            )
        return await db.fetch_one(query)

    async def get_by_ids(
        self, 
        oi_ids: List[int]
    ) -> List[OrderItem]:
        query = self.model.select().where(
            self.model.c.id.in_(oi_ids)
        )
        return await db.fetch_all(query)
        


order_item = CRUDOrderItem(order_item)