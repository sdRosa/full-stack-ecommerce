from sqlalchemy.orm import Session
from sqlalchemy import desc
from typing import Any, List, Optional, Tuple

from app.crud.base import CRUDBase
from app.models.item import Item, item
from app.schemas.item_sch import ItemSch, ItemCreate, ItemUpdate
from app.db.db_setup import db



ORDER_BY = {
    'datetime': item.c.timestamp,
    'sold': item.c.sold,
}

FILTER_BY = {
    'availability': item.c.availability,
    'is_active': item.c.is_active,
    # 'discount_price': item.c.discount_price,
    'featured': item.c.featured
}

class CRUDItem(CRUDBase[ItemSch, ItemCreate, ItemUpdate]):
    async def get_count(
        self
    ):
        query = self.model.count()
        return await db.fetch_val(query)

    
    async def get_count_by_cat(
        self,
        cat_id: int
    ):
        query = self.model.count().where(
            self.model.c.category_id == cat_id
        )
        return await db.fetch_val(query)


    async def get_by_filter(
        self,
        skip: int,
        limit: int,
        is_active: bool,
        filter: Tuple[str, bool]
    ) -> List[Item]:
        if filter in FILTER_BY:
            query = self.model.select.where(
                (self.model.c.is_active == is_active) &
                (FILTER_BY[filter[0]] == FILTER_BY[filter[1]])
            )
        obj_list = await db.fetch_all(query)
        return [dict(result) for result in obj_list]


    async def get_multi_order_by(
        self,
        skip: int,
        limit: int,
        is_active: bool,
        order_by: Optional[str] = None
    ) -> List[Item]:
        if order_by != None and order_by in ['datetime','sold']:
            query = self.model.select(
                self.model.c.is_active == is_active
            ).order_by(desc(ORDER_BY[order_by])).offset(skip).limit(limit)
        else:
            query = self.model.select(
                self.model.c.is_active == is_active
            ).offset(skip).limit(limit)

        obj_list = await db.fetch_all(query)
        return [dict(result) for result in obj_list]


    async def get_multi_by_cat(
        self,
        cat_id: int,
        skip: int,
        limit: int,
        is_active: bool,
        order_by: Optional[str] = None
    ) -> List[Item]:
        if order_by != None and order_by in ['datetime', 'sold']:
            query = self.model.select().order_by(
                desc(ORDER_BY[order_by])
            ).where(
                (self.model.c.is_active == is_active) &
                (self.model.c.category_id == cat_id)
            ).offset(skip).limit(limit)
        else:
            query = self.model.select().where(
                (self.model.c.is_active == is_active) &
                (self.model.c.category_id == cat_id)
            ).offset(skip).limit(limit)

        obj_list = await db.fetch_all(query)
        return [dict(result) for result in obj_list]


    async def create_with_image(
        self, 
        *, 
        obj_in: Session
    ) -> Optional[Item]:
        query = item.insert().values(**obj_in)
        return await db.execute(query)


    async def incr_stock(
        self,
        item_id: int,
        obj_in: Session
    ) -> Item:
        query = self.model.update().where(
            self.model.c.id == item_id
        ).values(**obj_in.dict()).returning(self.model.c.stock)
        return await db.fetch_one(query=query)


    async def update_in_place(
        self, 
        item_id: int, 
        obj_in: Any
    ) -> Item:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
            
        query = self.model.update().where(
            self.model.c.id == item_id
        ).values(**update_data).returning(self.model.c.stock)
        return await db.fetch_one(query=query)


item = CRUDItem(item)