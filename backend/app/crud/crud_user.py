from typing import Optional, Union

from app.crud.base import CRUDBase
from app.core.security import get_password_hash, verify_password
from app.models.user import user, User
from app.schemas.user_sch import UserSch, UserCreatePriv, UserUpdate
from app.db.db_setup import db


class CRUDUser(CRUDBase[UserSch, UserCreatePriv, UserUpdate]):
    async def get_by_email(
        self, 
        *, 
        email: str
    ) -> User:
        query = self.model.select().where(self.model.c.email == email)
        return await db.fetch_one(query)


    async def authenticate(
        self, 
        *, 
        email: str, 
        password: str
    ) -> Optional[User]:
        user = await self.get_by_email(email=email)
        if not user:
            return None
        if not verify_password(password, user['password']):
            return None
        return user


    async def create(
        self, 
        *, 
        obj_in: UserCreatePriv
    ) -> Optional[User]:
        db_obj = obj_in.copy(update={'password': get_password_hash(obj_in.password)})
        
        query = self.model.insert().values(**db_obj.dict())
        return await db.execute(query)


    async def is_active(self, user: User) -> bool:
        return user['is_active']


user = CRUDUser(user)