from typing import Any, List, Optional
from sqlalchemy import desc

from app.crud.base import CRUDBase
from app.models.address import Address, address
from app.schemas.address_sch import AddressSch, AddressCreatePriv, AddressUpdate
from app.db.db_setup import db


class CRUDAddress(CRUDBase[AddressSch, AddressCreatePriv, AddressUpdate]):
    async def get_multi(
        self, *, 
        skip: int = 0, 
        limit: int = 100, 
    ) -> List[Address]:
        query = self.model.select().offset(skip).limit(limit)

        obj_list = await db.fetch_all(query)
        return [dict(result) for result in obj_list]


    async def get_own_addresses(
        self,
        user_id: int,
        skip: int,
        limit: int
    ) -> List[Address]:
        query = self.model.select(
            self.model.c.user_id == user_id
        ).order_by(desc(self.model.c.id)).offset(skip).limit(limit)
        
        obj_list = await db.fetch_all(query)
        return [dict(result) for result in obj_list]

    async def get_latest(
        self,
        user_id: int
    ) -> Address:
        query = self.model.select(
            self.model.c.user_id == user_id
        ).order_by(desc(self.model.c.id))
        
        return await db.fetch_one(query)


    async def get_by_cus(
        self,
        customer_id: int,
    ) -> Address:
        """ Get Address by customer_id """
        query = self.model.select().where(
            self.model.c.customer_id == customer_id
        )
        return await db.fetch_one(query)


address = CRUDAddress(address)