from typing import List, Any
from sqlalchemy.sql import select

from app.crud.base import CRUDBase
from app.models.category import category, Category
from app.schemas.category_sch import CategorySch, CategoryCreate, CategoryUpdate
from app.db.db_setup import db


class CRUDCat(CRUDBase[CategorySch, CategoryCreate, CategoryUpdate]):
    async def get_cat_names(
        self, 
        skip: int = 0,
        limit: int = 10
    ) -> Any:
        query = select([self.model.c.id, self.model.c.name]).offset(skip).limit(limit)
        obj_list = await db.fetch_all(query)
        return [dict(result) for result in obj_list]

    async def get_cat_name(
        self,
        cat_id: str
    ):
        query = select([self.model.c.name]).where(self.model.c.id == cat_id)
        return await db.fetch_val(query)


cat = CRUDCat(category)