from typing import List, Optional

from app.crud.base import CRUDBase
from app.models.order import OIAssociation, order_bound
from app.schemas.order_asso_sch import OrderAssoSch, OrderAssoCreate, OrderAssoUpdate
from app.db.db_setup import db
 

class CRUDOrderItemAsso(CRUDBase[OrderAssoSch, OrderAssoCreate, OrderAssoUpdate]):
    async def create_bulk(
        self, 
        bulk_obj: 
        List[OrderAssoCreate]
    ) -> List[Optional[OIAssociation]]:
        if len(bulk_obj) > 1:
            query = self.model.insert().values(bulk_obj).returning(self.model.c.id)
            return await db.fetch_all(query)
    
        query = self.model.insert().values(**bulk_obj[0])
        return await db.execute(query)

    async def get_by_oi(
        self,
        oi_id,
    ) -> OIAssociation:
        query = self.model.select().where(
            self.model.c.order_items_id == oi_id
        )
        return await db.fetch_one(query)

    async def get_by_order_oi(
        self,
        order_id,
        oi_id,
    ) -> OIAssociation:
        query = self.model.select().where(
            (self.model.c.order_id == order_id) &
            (self.model.c.order_items_id == oi_id)
        )
        return await db.fetch_one(query)

    async def get_multi_by_order(
        self,
        order_id,
    ) -> List[OIAssociation]:
        query = self.model.select().where(
            self.model.c.order_id == order_id
        )
        obj_list = await db.fetch_all(query)
        return [dict(result) for result in obj_list] 


orders_asso = CRUDOrderItemAsso(order_bound)