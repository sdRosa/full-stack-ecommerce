from sqlalchemy import desc
from typing import List, Optional

from app.crud.base import CRUDBase
from app.models.order import Order, order
from app.schemas.order_sch import OrderSch, OrderCreatePriv, OrderUpdate
from app.db.db_setup import db


ORDER_BY = {
    'created_at': order.c.created_at,
    'subtotal': order.c.subtotal,
}
 

class CRUDOrder(CRUDBase[OrderSch, OrderCreatePriv, OrderUpdate]):
    async def create(
        self, 
        obj_in: OrderCreatePriv
    ) -> Order:
        query = self.model.insert().values(**obj_in.dict())
        return await db.fetch_one(query)

    # async def get_multi(
    #     self, *, 
    #     skip: int = 0, 
    #     limit: int = 100, 
    # ) -> List[Order]:
    #     query = self.model.select().offset(skip).limit(limit)

    #     obj_list = await db.fetch_all(query)
    #     return [dict(result) for result in obj_list]


    async def get_own_multi(
        self, *,
        user_id: int, 
        skip: int = 0, 
        limit: int = 100
    ) -> List[Order]:
        query = self.model.select().where(
            self.model.c.user_id == user_id
        ).offset(skip).limit(limit)

        obj_list = await db.fetch_all(query)
        
        return [dict(result) for result in obj_list]


    async def get_count(
        self,
        user_id: int
    ) -> int:
        query = self.model.count().where(
            self.model.c.user_id == user_id
        )
        return await db.fetch_val(query)


    async def get_multi_order_by(
        self,
        skip: int,
        limit: int,
        order_by: Optional[str] = 'created_at'
    ) -> List[Order]:
        if order_by != None and order_by in ['created_at', 'subtotal']:
            query = self.model.select(
            ).order_by(desc(ORDER_BY[order_by])).offset(skip).limit(limit)
        else:
            query = self.model.select().offset(skip).limit(limit)

        obj_list = await db.fetch_all(query)
        return [dict(result) for result in obj_list]

  

order = CRUDOrder(order)