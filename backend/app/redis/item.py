from pydantic.types import condecimal
from typing import Dict
from aioredis import Redis

import asyncio
import json

from app.schemas.redis import rd_item_sch
from app.redis.base import BaseRedisService
from app.redis.exceptions import ErrorMessage, ItemDoesNotExists
from app.redis.decorators import raise_exception


TTL = 604800


class Item(BaseRedisService):

    """
        Redis service for Items. Mainly used for handling transactions.
    """

    def __init__(
        self,
        redis_connection: Redis,
        item_id: int,
        ttl: int = TTL
    ):
        self.__redis_prefix='item'
        self.item_id = item_id
        item_key = self.__get_item_redis_key(
            item_id,
        )
        super().__init__(
            redis_connection, item_key, ttl)


    def __get_item_redis_key(
        self,
        item_id: int 
    ):
        return "{}/{}".format(
            self.__redis_prefix, 
            item_id
        )


    @raise_exception("Item can't be added due to Error: ")
    async def add(
        self,
        item_in: rd_item_sch.RDItemCreate,
        **extra_data_dict
        ) -> bool:
        """
            Returns True if the addition of the obj of the given item_id 
            is succesful else False.
            Can also add extra data in the form of dictionary.
        """
        if extra_data_dict:
            item_in.extra_data_dict = extra_data_dict
        item_dict = item_in.dict()
        task1 = self.redis_connection.hset(
            self.obj_redis_key, item_in.item_id, json.dumps(item_dict)
        )
        task2 = self.set_ttl()
        rp, _ = await asyncio.gather(task1, task2)

        self.obj_exists_in = await self.obj_exists()
        return rp

    @raise_exception("Product dictionaries can't be obatined due to Error: ")
    async def get_item_dicts(self):
        """
            Returns the list of all product details
        """
        get_items = await self.redis_connection.hvals(self.obj_redis_key)
        return [json.loads(item_string) for item_string in get_items]


    @raise_exception("remove quantities can't function due to Error: ")
    async def remove_quantities(
        self,
        item_id: int, 
        item_in: rd_item_sch.RDItem, 
        quantity: int = 1,
        ) -> int:
        """
            Removes one or more quantities of a item from the obj
        """
        less_bucket = item_in.pg_stock = item_in.pg_stock - quantity

        item_dict = item_in.dict()

        if less_bucket <= 0:
            item_in.pg_stock = 0
            task_1 = self.redis_connection.hset(
                self.obj_redis_key, item_id, json.dumps(item_dict)
            )
            task_2 =self.set_ttl()
            asyncio.gather(task_1, task_2)
            return 0

        task_1 = self.redis_connection.hset(
            self.obj_redis_key, item_id, json.dumps(item_dict)
        )
        task_2 = self.set_ttl()
        asyncio.gather(task_1, task_2)

        return less_bucket


    async def update_item_quantity(
        self, 
        item_id: int,
        item_in: rd_item_sch.RDItem, 
        quantity: int = 1
    ) -> int:
        """
            Update Cart item quantity.
        """
        
        new_stock = item_in.pg_stock = item_in.pg_stock + quantity

        task_1 = self.redis_connection.hset(
            self.obj_redis_key, item_id, json.dumps(item_in.dict()))
        task_2 = self.set_ttl()
        asyncio.gather(task_1, task_2)

        return new_stock
