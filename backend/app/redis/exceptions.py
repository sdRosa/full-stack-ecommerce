class ErrorMessage(Exception):

    def __init__(self, msg):
        self.value = msg

    def __str__(self):
        return repr(self.value)


class OutOfStockError(Exception):
    """Raised when quantity < 0"""


class QuantityOutOfStockError(Exception):
    """Raised when an item was fetched on cart but there is no more of them"""


class CartDoesNotExists(ErrorMessage):
    """El carrito no existe actualmente o ha expirado"""


class ItemDoesNotExists(ErrorMessage):
    """El producto no existe actualmente en stock"""
