from pydantic.types import condecimal
from typing import Optional, List, Dict

from app.redis.base import BaseRedisService
from app.schemas.redis import rd_cart_sch
from app.redis.exceptions import ErrorMessage, CartDoesNotExists, ItemDoesNotExists
from app.redis.decorators import raise_exception

from aioredis import Redis

import json
import asyncio

from functools import reduce
from decimal import Decimal


TTL = 604800


class Cart(BaseRedisService):

    """
        Redis service for Carts
    """
    def __init__(
        self,
        redis_connection: Redis,
        customer_id: int,
        ttl: int = TTL
    ):
        self.__redis_prefix='cart'
        self.customer_id = customer_id
        cart_key = self.__get_cart_redis_key(
            customer_id
        )
        super().__init__(redis_connection, cart_key, ttl)


    def __get_cart_redis_key(
        self, 
        customer_id: int 
    ):
        return "{}/{}".format(
            self.__redis_prefix, 
            customer_id
        )


    @raise_exception("contains can't function due to Error: ")
    async def contains(self, item_id):
        """
            Checks whether the given product exists in the cart
        """
        return await self.redis_connection.hexists(self.obj_redis_key, item_id)


    async def __get_raw_cart(self):
        """
            Return binaries items of hgetall from Redis conn
        """
        return await self.redis_connection.hgetall(
            self.obj_redis_key)


    @raise_exception("Cart can't be obtained due to Error: ")
    async def get(self):
        """
            Returns all the products and their details present in the cart as a dictionary
        """
        carts = await self.__get_raw_cart()
        return {key: json.loads(value) for key, value in carts.items()}


    @raise_exception("count can't be obtained due to Error: ")
    async def count(self) -> int:
        """
            Returns the number of types of products in the carts
        """
        return await self.redis_connection.hlen(self.obj_redis_key)


    @raise_exception("Product dictionaries can't be obatined due to Error: ")
    async def get_item_dicts(self):
        """
            Returns the list of all product details
        """
        get_items = await self.redis_connection.hvals(self.obj_redis_key)
        return [json.loads(item_string) for item_string in get_items]

    
    async def get_required_vals(self, *lst):
        """
            Returns a list of selected required vals
        """
        get_items = await self.redis_connection.hmget(self.obj_redis_key, *lst)
        return [json.loads(item_string) for item_string in get_items]


    async def get_item_dicts_bulk(self, lookup: Optional[List] = None):
        """
            Returns a list of dicts with the specific members to make a
            DB bulk
        """
        get_items = await self.redis_connection.hvals(self.obj_redis_key)
        items_list = [json.loads(item_string) for item_string in get_items]
        lookup_lst = lookup or ['item_id', 'quantity']
        
        required_keys = []
        for i in items_list:
            new_dict = { your_key: i[your_key] for your_key in lookup_lst}
            required_keys.append(new_dict)

        return required_keys


    @raise_exception("Cart can't be added due to Error: ")
    async def add(
        self,
        cart_in: rd_cart_sch.RDCartCreate,
        **extra_data_dict
        ) -> bool:
        """
            Returns True if the addition of the Cart of the given item_id 
            is succesful else False.
            Can also add extra data in the form of dictionary.
        """
        if extra_data_dict:
            cart_in.extra_data_dict = extra_data_dict
        
        task1 = self.redis_connection.hset(
            self.obj_redis_key, cart_in.item_id, json.dumps(cart_in.dict())
        )
        task2 = self.set_ttl()
        rp, _ = await asyncio.gather(task1, task2)

        self.obj_exists_in = await self.obj_exists()
        return rp


    @raise_exception("remove quantities can't function due to Error: ")
    async def remove_quantities(
        self,
        item_id: int, 
        item_in: dict, 
        quantity: int = 1
        ) -> Dict:
        """
            Removes one or more quantities of a item from the obj
        """
        new_stock = item_in['quantity'] = item_in['quantity'] - quantity
        if new_stock <= 0:
            del_obj = await self.redis_connection.hdel(self.obj_redis_key, item_id)
            if del_obj:
                await self.set_ttl()
                return {'new_stock': 0,'deleted': True}
            else:
                return {'new_stock': new_stock,'deleted': False}

        task_1 = self.redis_connection.hset(
            self.obj_redis_key, item_id, json.dumps(item_in)
        )
        task_2 = self.set_ttl()
        asyncio.gather(task_1, task_2)
        return {'new_stock': new_stock, 'deleted': False}

    
    async def update_cart_quantity(
        self, 
        item_id: int, 
        quantity: int = 1
    ) -> int:
        """
            Update Cart item quantity.
        """
        
        raw_item = await self.get_obj(item_id)
        item_in = rd_cart_sch.RDCart(**raw_item)

        if not item_in:
            raise ItemDoesNotExists("Item does not exists")

        new_quantity = item_in.quantity = item_in.quantity + quantity

        task_1 = self.redis_connection.hset(
            self.obj_redis_key, item_id, json.dumps(item_in.dict()))
        task_2 = self.set_ttl()
        asyncio.gather(task_1, task_2)

        return new_quantity


    async def __quantities(self):
        item_dicts = await self.get_item_dicts()
        return map(lambda item_dict: item_dict.get('quantity'), item_dicts)


    @raise_exception("quantity can't be obtained due to Error: ")
    async def quantity(self) -> int:
        """
            Returns the total number of units of all products in the cart
        """
        quantities = await self.__quantities()
        return reduce(lambda result, quantity: quantity + result, quantities)


    @raise_exception("total_cost can't be obatined due to Error: ")
    async def subtotal_cost(self) -> Decimal:
        """
            Returns the subtotal of all product cost from the cart
        """
        price_list = await self.__price_list()
        return sum(price_list)


    def __item_price(self, item_dict):
        """
            Returns the product of product_quantity and its price
        """
        return item_dict['quantity'] * Decimal(item_dict['unit_price'])


    async def __price_list(self):
        """
            Returns the list of product's total_cost
        """
        item_dicts = await self.get_item_dicts()
        return map(lambda item_dict: Decimal(self.__item_price(item_dict)), item_dicts)


    async def delete_carts(self):
        """
            Deletes the user's cart
        """
        return await self.redis_connection.delete(self.obj_redis_key)

        