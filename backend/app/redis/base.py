from typing import Dict, List

from pydantic.types import condecimal

from app.redis.exceptions import ErrorMessage, ItemDoesNotExists
from app.redis.decorators import raise_exception

import json
import asyncio

from aioredis.commands import Redis
from decimal import Decimal


TTL = 604800


class BaseRedisService(object):

    """
        Redis Base Class for Cart and Item services, with basic utilities.
    """

    @raise_exception("Service can't be initialized due to Error: ")
    def __init__(
        self,
        redis_connection: Redis,
        redis_key: str,
        ttl: int = TTL
        ):
        """
            Constructor for the class, initializes redis conn,
            ttl, and redis_key for objects.
            
        """
        self.redis_connection = redis_connection
        self.obj_redis_key    = redis_key
        self.ttl = ttl


    async def _init(self):
        """
            Initializes aditional required members in asyncronycal way
        """
        self.obj_exists_in = await self.obj_exists()


    @raise_exception("ttl can't be set due to Error: ")
    async def set_ttl(self, expiry_time=TTL):
        """
            Update the ttl of the cart
        """
        return await self.redis_connection.expire(self.obj_redis_key, expiry_time)


    @raise_exception("ttl can't be obtained due to Error: ")
    async def get_ttl(self):
        """
            Get the ttl of the obj
        """
        ttl = await self.redis_connection.ttl(self.obj_redis_key)
        if ttl:
            return ttl
        else:
            # raise ObjDoesNotExists("Customer Cart does not exists")
            pass


    @raise_exception("Object exists can't return a value due to Error: ")
    async def obj_exists(self) -> bool:
        """
            Confirm obj hash in Redis
        """
        return await self.redis_connection.exists(self.obj_redis_key)

 
    @raise_exception("Obj can't be obtained due to Error: ")
    async def get_obj(self, item_id, force_query=False):
        """
            Returns the obj details as a Dictionary for the given item_id.
        """
        if force_query or self.obj_exists_in:
            item_string = await self.redis_connection.hget(
                self.obj_redis_key, item_id)
            if item_string:
                return json.loads(item_string)
            else:
                return {}
        else:
            return {}


    @raise_exception("remove can't function due to Error: ")
    async def remove(self, item_id):
        """
            Removes the item from the obj
        """
        del_item = await self.redis_connection.hdel(self.obj_redis_key, item_id)
        if del_item:
            await self.set_ttl()
            return True
        else:
            return False


    @raise_exception("remove quantities can't function due to Error: ")
    async def remove_quantities(
        self,
        item_id: int, 
        item_in: dict, 
        quantity: int = 1
        ) -> Dict:
        """
            Removes one or more quantities of a item from the obj
        """
        less_quantity = item_in['quantity'] = item_in['quantity'] - quantity

        if less_quantity <= 0:
            del_obj = await self.redis_connection.hdel(self.obj_redis_key, item_id)
            if del_obj:
                await self.set_ttl()
                return {'deleted': True}
            else:
                return {'deleted': False}

        task_1 = self.redis_connection.hset(
            self.obj_redis_key, item_id, json.dumps(item_in)
        )
        task_2 = self.set_ttl()
        asyncio.gather(task_1, task_2)

        return {'less_quantity': less_quantity, 'deleted': False}
    