from app.core.config import settings

from typing import Any
import redis
import aioredis

class RedisConnPool(object):
    
    def __init__(
        self,
        host_name: str = settings.REDIS_HOST, 
        port: int = settings.REDIS_PORT, 
        db: int = settings.REDIS_DB,
        password: Any = None,
        ):
        self.password = password
        self.host = host_name
        self.port = port
        self.db = db

    async def create_redis_pool(self):
        if self.password:
            return await aioredis.create_redis_pool(
                f'redis://:{self.password}@{self.host}/{self.db}?encoding=utf-8')

        return await aioredis.create_redis_pool(
                f'redis://{self.host}/{self.db}?encoding=utf-8')


class RedisConn(object):
    
    def __init__(
        self,
        host_name: str = settings.REDIS_HOST, 
        port: int = settings.REDIS_PORT, 
        db: int = settings.REDIS_DB,
        password: Any = None,
        ):
        self.password = password
        self.host = host_name
        self.port = port
        self.db = db

    # def create_redis_sync(self):
    #     if self.password:
    #         return 

    async def create_redis(self):
        if self.password:
            return await aioredis.create_redis(
                f'redis://:{self.password}@{self.host}/{self.db}?encoding=utf-8',
                password=self.password)

        return await aioredis.create_redis(
                f'redis://{self.host}/{self.db}?encoding=utf-8')

rd = RedisConn()