from typing import Any, List

from fastapi import APIRouter, Security, Response, status
from fastapi.responses import JSONResponse

from app.api import deps
from app.crud import crud_cat
from app.schemas.msg_sch import Message, ErrorTypeEnum
from app.schemas import category_sch, user_sch

 
router = APIRouter()


@router.post(
    "/", 
    responses={
        status.HTTP_201_CREATED: {
            'model': category_sch.CategorySch, 
            'description': 'The obj was created/updated successfully'
        },
        status.HTTP_400_BAD_REQUEST: {
            'model': Message, 
            'description': 'The obj already exists'
        },
    } 
)
async def create_category(
    *,
    cat_in: category_sch.CategoryCreate,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"]),
    rp: Response
) -> Any:
    cat = await crud_cat.cat.get_by_name(name=cat_in.name)
    if cat:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST, 
            content={
                'msg': f'La categoria "{cat_in.name}" ya existe, intente con otro nombre', 
                'type': ErrorTypeEnum.warning
            })
    cat_id = await crud_cat.cat.create(obj_in=cat_in)
    cat_rp = category_sch.CategorySch(**cat_in.dict(exclude_none=True), id=cat_id)
    rp.status_code = status.HTTP_201_CREATED
    return cat_rp
    

@router.get("/all", response_model=List[category_sch.CategorySch])
async def get_categories(
    skip: int = 0,
    limit: int = 100, 
    is_active: bool = False,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me"])
) -> Any:
    """
    Get a List of Categories with skip, limit and is_active params
    """
    categories = await crud_cat.cat.get_multi(
        skip=skip, 
        limit=limit, 
        is_active=is_active,
    )
    return categories


@router.get(
    "/all-names", 
    response_model=List[category_sch.CategoryNames],
    response_model_exclude_none=True,
)
async def get_categories_names(
    skip: int = 0,
    limit: int = 100,
) -> Any:
    """
    Get a List of Categories names with skip and limit
    """
    categories = await crud_cat.cat.get_cat_names(
        skip=skip, 
        limit=limit, 
    )
    return categories


@router.get(
    "/name/{cat_id}", 
    response_model=str,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': Message, 
            "description": "The obj doesn't exists"
        },
    } 
)
async def get_category_name(
    cat_id: int
) -> Any:
    """
    Get a name of category by id
    """
    cat_name = await crud_cat.cat.get_cat_name(cat_id)
    if cat_name:
        return cat_name
    else:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': f'La categoria con el ID "{cat_id}" no existe', 
                'type': ErrorTypeEnum.warning
            })
 

@router.get(
    "/{cat_id}", 
    response_model=category_sch.CategoryInDB,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': Message, 
            "description": "The obj doesn't exists"
        },
    } 
)
async def get_cat_by_id(
    cat_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me"]),
) -> Any:
    """
    Get Category by id
    """
    cat = await crud_cat.cat.get(id=cat_id)
    if not cat:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': f'La categoria con el ID "{cat_id}" no existe', 
                'type': ErrorTypeEnum.warning
            })
    cat_rp = category_sch.CategoryInDB(**cat)
    return cat_rp
    

@router.patch(
    "/{cat_id}",
    response_model=category_sch.CategorySch,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': Message, 
            "description": "The obj doesn't exists"
        },
    } 
)
async def update_cat(
    *,
    cat_id: int,
    cat_in: category_sch.CategoryUpdate,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"])
) -> Any:
    """
    Update a Category object
    """
    cat = await crud_cat.cat.get(id=cat_id)
    if not cat:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': f'La categoria con el ID "{cat_id}" no existe', 
                'type': ErrorTypeEnum.warning
            })
    cat_sch = category_sch.CategoryInDB(**cat)
    updated_cat = await crud_cat.cat.update(db_obj=cat_sch, obj_in=cat_in)

    cat_sch = category_sch.CategorySch(**updated_cat)
    return cat_sch
 

@router.delete(
    "/{cat_id}",
    response_model=Message,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': Message, 
            "description": "The obj doesn't exists"
        },
    } 
)
async def delete_cat(
    *,
    cat_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"])
) -> Any:
    """
    Delete a Category with CASCADE on items delete
    """
    cat = await crud_cat.cat.get(id=cat_id)
    if not cat:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': f'La categoria con el ID "{cat_id}" no existe', 
                'type': ErrorTypeEnum.warning
            })
    await crud_cat.cat.remove(id=cat_id)
    del_msg = {
        'msg': f"Categoria '{cat['name']}' borrada exitosamente", 
        'type': ErrorTypeEnum.success
    }
    return Message(**del_msg)
