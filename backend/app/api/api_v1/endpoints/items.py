from typing import Any, List, Optional
import math

from fastapi import (
    APIRouter, 
    HTTPException, 
    UploadFile,
    status, 
    File, 
    Form, 
    Security,
    Depends)
from fastapi import responses
from fastapi.responses import Response, JSONResponse
from pydantic import condecimal
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY


from app.api import deps
from app.crud import crud_item, crud_cat
from app.core.config import settings
from app.redis.item import Item
from app.redis.connection import RedisConn
from app.schemas import item_sch, user_sch
from app.schemas.msg_sch import Message, ErrorTypeEnum

import asyncio
import datetime
import shutil

router = APIRouter()


def get_pagination(size: int, page: int= 0):
    limit = size 
    offset = page * limit

    return {'limit': limit, 'offset': offset}

async def get_pagination_data(
    data: item_sch.ItemCount,
    page: int,
    limit: int
) -> item_sch.ItemsInShop:
    current_page = page 
    total_pages = math.ceil(data['count'] / limit)

    return {
        'total_items': data['count'], 
        'items': data['items'], 
        'total_pages': total_pages,
        'current_page': current_page
        }


@router.post(
    "/",
    responses={
        status.HTTP_201_CREATED: {
            'model': item_sch.ItemSch, 
            "description": "The obj was created successfully"
        },
        status.HTTP_404_NOT_FOUND: {
            'model': Message, 
            "description": "The obj doesn't exists"
        },
        status.HTTP_400_BAD_REQUEST: {
            'model': Message, 
            "description": "The obj already exists"
        },
        status.HTTP_406_NOT_ACCEPTABLE: {
            'model': Message, 
            "description": "The file format is wrong. Please, use jpeg or png"
        },
    }
)
async def create_item(
    *,
    name: str = Form(...),
    description: str = Form(...),
    availability: bool = Form(...),
    is_active: bool = Form(...),
    featured: bool = Form(...),
    stock: int = Form(...),
    unit_price: condecimal(decimal_places=2) = Form(...),
    discount_price: condecimal(decimal_places=2) = Form(...),
    category_id: int = Form(...),
    file: UploadFile = File(...),
    rp: Response,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"],
    )
) -> Any:
    item = await crud_item.item.get_by_name(name=name)
    if item:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST, 
            content={
                'msg': f'El item con el nombre "{name}" ya existe, intente con otro', 
                'type': ErrorTypeEnum.warning
            })
    cat = await crud_cat.cat.get(id=category_id) 
    if not cat:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'La categoria seleccionada no existe"', 
                'type': ErrorTypeEnum.warning
            })
    
    if file.content_type not in ['image/jpeg', 'image/png']:
        return JSONResponse(
            status_code=status.HTTP_406_NOT_ACCEPTABLE, 
            content={
                'msg': 'Formato de imagen incorrecto. Usar .jpeg o .png', 
                'type': ErrorTypeEnum.warning
            })
    file_location = f'static/{file.filename}'
    with open(file_location, "wb+") as file_object:
        shutil.copyfileobj(file.file, file_object) 

    raw_item = {
        'name': name,
        'description': description,
        'timestamp': datetime.datetime.now(),
        'availability': availability,
        'is_active': is_active,
        'image_url': file_location,
        'category_id': category_id,
        'stock': stock,
        'unit_price': unit_price,
        'discount_price': discount_price,
        'featured': featured,
    }
    item_in = item_sch.ItemCreate(**raw_item)
    item_id = await crud_item.item.create(obj_in=item_in)
    rp.status_code = status.HTTP_201_CREATED
    return item_sch.ItemSch(**item_in.dict(), id=item_id)


@router.get("/all", response_model=List[item_sch.ItemInDB])
async def get_items(
    skip: int = 0,
    limit: int = 100,
    is_active: bool = True,
    order_by: Optional[str] = None,
) -> Any:
    items = await crud_item.item.get_multi_order_by(
        skip=skip, 
        limit=limit, 
        is_active=is_active,
        order_by=order_by
    )
    return items


@router.get("/shop", response_model=item_sch.ItemsInShop)
async def get_shop_items(
    page: int = 0,
    size: int = 5,
    order_by: Optional[str] = None,
    is_active: bool = True
):
    limit, offset = get_pagination(size, page).values()
    items_count = await crud_item.item.get_count()
    items = await crud_item.item.get_multi_order_by(
        limit=limit, skip=offset, order_by=order_by, is_active=is_active
    )

    return await get_pagination_data(
        {'count': items_count, 'items': items}, 
        page,
        limit
    )


@router.get(
    "/all-by-cat/{cat_id}", 
    response_model=item_sch.ItemsByCat,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': Message, 
            "description": "The category doesn't exists"
        },
    }
)
async def get_items_by_cat(
    cat_id: int,
    skip: int = 0,
    limit: int = 100,
    is_active: bool = True,
) -> Any:
    cat = await crud_cat.cat.get(cat_id)
    if not cat:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'La categoria seleccionada no existe"', 
                'type': ErrorTypeEnum.warning
            })
    items = await crud_item.item.get_multi_by_cat(
        cat_id=cat_id,
        skip=skip, 
        limit=limit, 
        is_active=is_active, 
    )
    return {'cat_name': cat['name'], 'items': items}

@router.get(
    "/shop-cat/{cat_id}", 
    response_model=item_sch.ItemsInShop,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': Message, 
            "description": "The category doesn't exists"
        },
    }
)
async def get_shop_items_by_cat(
    cat_id: int,
    page: int = 0,
    size: int = 5,
    order_by: Optional[str] = None,
    is_active: bool = True
) -> Any:
    limit, offset = get_pagination(size, page).values()
    items_count = await crud_item.item.get_count_by_cat(cat_id)
    items = await crud_item.item.get_multi_by_cat(
        cat_id=cat_id,
        skip=offset, 
        limit=limit,  
        order_by=order_by, 
        is_active=is_active
    )
    return await get_pagination_data(
        {'count': items_count, 'items': items}, 
        page,
        limit
    )
 

@router.get(
    "/{item_id}", 
    response_model=item_sch.ItemInDB,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': Message, 
            "description": "The obj doesn't exists"
        },
    }
)
async def get_item_by_id(
    item_id: int,
) -> Any:
    item = await crud_item.item.get(id=item_id)
    if not item:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'El item seleccionada no existe"', 
                'type': ErrorTypeEnum.warning
            })
    rp = item_sch.ItemInDB(**item)
    return rp
    

@router.patch(
    "/{item_id}",
    responses={
        status.HTTP_201_CREATED: {
            'model': item_sch.ItemSch, 
            "description": "The obj was updated successfully"
        },
        status.HTTP_404_NOT_FOUND: {
            'model': Message, 
            "description": "The obj doesn't exists"
        },
    }
)
async def update_item(
    *,
    item_id: int,
    item_in: item_sch.ItemUpdate,
    rp: Response,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"])
) -> Any:
        
    item = await crud_item.item.get(id=item_id)
    if not item:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'El item seleccionada no existe"', 
                'type': ErrorTypeEnum.warning
            })
    item_schema = item_sch.ItemInDB(**item)
    if item_in.stock != None:
        
        open_conn = deps.RedisConn()
        conn = await open_conn.create_redis()
        item_rd = Item(
            redis_connection=conn,
            item_id=item_id
        )
        await item_rd._init()

        if item_rd.obj_exists_in:
            item_in_rd = item_rd.get_obj(item_rd.item_id)
            if item_in.stock_bucket and item_schema.stock_bucket != item_in.stock_bucket:
                # validate item_in: can't be less than 0
                task_1 = item_rd.update_item_quantity(
                    item_rd.item_id, item_in_rd, quantity=item_in.stock_bucket)
                task_2 = crud_item.item.update(db_obj=item_schema, obj_in=item_in)

                _, item = await asyncio.gather(task_1, task_2)
                return item
    
    item = await crud_item.item.update(db_obj=item_schema, obj_in=item_in)
    rp.status_code = status.HTTP_201_CREATED
    return item


@router.patch(
    "/increment-stock/{item_id}",
    responses={
        status.HTTP_201_CREATED: {
            'model': item_sch.ItemSch, 
            "description": "The obj was updated successfully"
        },
        status.HTTP_404_NOT_FOUND:{
            "model": Message,
            "description": "The obj doesn't exists"
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY:{
            "model": Message,
            "description": "The stock_in can't recieve 0 or negatives values"
        }
    }
)
async def increment_stock(
     stock_in: item_sch.UpdateStock,
     rp: Response,
     item_rd: Item = Depends(deps.init_redis_item_conn),
     current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"])
 ) -> Any:
    item = await crud_item.item.get(id=item_rd.item_id)

    if not item:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'El item seleccionada no existe"', 
                'type': ErrorTypeEnum.warning
            })
    elif stock_in.stock == 0 or stock_in.stock < 0:
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
            content={
                'msg': 'El stock no puede ser 0 o menor a 0', 
                'type': ErrorTypeEnum.warning
            })
    db_item = item_sch.ItemInDB(**item)
 
    db_item.stock = db_item.stock + stock_in.stock

    if item_rd.obj_exists_in:
        # if stock_in.stock_bucket and db_item.stock_bucket != stock_in.stock_bucket:
            # validate item_in: can't be less than 0
        item_in_rd = item_rd.get_obj(item_rd.item_id)
        task_1 = item_rd.update_item_quantity(
            item_rd.item_id,
            item_in_rd, 
            bucket=stock_in.stock
        )
        task_2 = crud_item.item.incr_stock(
            item_rd.item_id,
            obj_in=db_item
        )
        _, item = await asyncio.gather(task_1, task_2)
        rp.status_code = status.HTTP_201_CREATED
        return item
    
    item = await crud_item.item.incr_stock(
            item_rd.item_id,
            obj_in=db_item
        )
    rp.status_code = status.HTTP_201_CREATED
    return item


@router.delete(
    "/{item_id}",
    response_model=Message,
    responses={
        status.HTTP_404_NOT_FOUND:{
            "model": Message,
            "description": "The obj doesn't exists"
        },
    }
)
async def delete_item(
    *,
    item_id: int, 
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"])
) -> Any:
    item = await crud_item.item.get(id=item_id)
    if not item:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'El item seleccionada no existe"', 
                'type': ErrorTypeEnum.warning
            })
    item = await crud_item.item.remove(id=item_id)
    del_msg = {
        'msg': "Item eliminado exitosamente", 
        'type': ErrorTypeEnum.success
    }
    return Message(**del_msg)

