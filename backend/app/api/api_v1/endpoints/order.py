from decimal import Decimal
from fastapi import APIRouter, Security, status
from fastapi.responses import JSONResponse
from typing import List, Any, Optional
import asyncio
import math

from pydantic.types import condecimal

from app.api import deps
from app.crud import (
    crud_address,
    crud_item,
    crud_order,
    crud_order_assotion, 
    crud_order_items)
from app.db.db_setup import db
from app.schemas import (
    address_sch,
    order_item_sch, 
    order_sch,
    order_asso_sch, 
    user_sch)
from app.schemas.msg_sch import Message, ErrorTypeEnum
from app.redis.connection import RedisConn
from app.redis.cart import Cart
from app.redis.item import Item as RedisItem


router = APIRouter()


def get_pagination(size: int, page: int= 0):
    limit = size 
    offset = page * limit

    return {'limit': limit, 'offset': offset}

async def get_pagination_data(
    data: order_sch.OrderCount,
    page: int,
    limit: int
) -> order_sch.OrdersPaginated:
    current_page = page 
    total_pages = math.ceil(data['count'] / limit)

    return {
        'total_orders': data['count'], 
        'orders': data['orders'], 
        'total_pages': total_pages,
        'current_page': current_page
        }


@router.post(
    "/bulk-cart",
    response_model=order_sch.OrderId, 
    response_model_exclude_none=True,
    responses={
        status.HTTP_404_NOT_FOUND:{
            "model": Message,
            "description": "The obj doesn't exists"
        },
        status.HTTP_405_METHOD_NOT_ALLOWED:{
            "model": Message,
            "description": "First the user should have an address"
        },
        status.HTTP_502_BAD_GATEWAY:{
            "model": Message,
            "description": "One of the items being pushed raised an error"
        },
    }
)
async def create_order(
    order_in: order_sch.OrderCreatePub,
    use_default_address: bool = False,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "cashier_admin"])
) -> Any:
    open_conn = RedisConn()
    conn = await open_conn.create_redis()
    cart = Cart(
        redis_connection=conn, 
        customer_id=current_user['id'], 
    )

    await cart._init()
 
    if not cart.obj_exists_in:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'No hay ningún carrito creado actualmente', 
                'type': ErrorTypeEnum.warning
            })
 
    items_in_cart = await cart.get_item_dicts()

    transaction = await db.transaction()
    try:
        stock_tasks, oi_required = [], []
        
        subtotal = await cart.subtotal_cost()
        
        order_priv = order_sch.OrderCreatePriv(
            **order_in.dict(),
            subtotal=subtotal,
            )

        for i in items_in_cart:
            redis_item = RedisItem(
                redis_connection=conn,
                item_id=i['item_id'],
            )
            await redis_item._init()
            
            item_qs = await redis_item.get_obj(i['item_id'], force_query=True)
            if not item_qs:
                raise Exception('not pushing some item')

            new_stock = {
                'stock': item_qs['pg_stock'] - i['quantity'],
                'sold': item_qs['sold'] + i['quantity']
            } 

            order_item = {
                'item_id': i['item_id'],
                'user_id': current_user['id'],
                'quantity': i['quantity']
            }
            
            oi_required.append(order_item)

            stock_tasks.append(
                crud_item.item.update_in_place(
                    i['item_id'],
                    new_stock
                )
            )

        # Create a new Address copying the old one so it could be
        # updated only for this order
        if use_default_address:
            if current_user['default_address'] and current_user['address_id']:
                default_address = await crud_address.address.get(
                    current_user['address_id'],
                )
                create_address = address_sch.AddressCreatePriv(**default_address)
                address_id = await crud_address.address.create(obj_in=create_address)
                order_priv.address_id = address_id
            
            else:
                return JSONResponse(
                    status_code=status.HTTP_404_NOT_FOUND, 
                    content={
                        'msg': 'No hay una dirección por defecto asociada', 
                        'type': ErrorTypeEnum.warning
                })
        
        else:
            recent_address = await crud_address.address.get(
                order_in.address_id,
            )
            if recent_address:
                create_address = address_sch.AddressCreatePriv(**recent_address)
                address_id = await crud_address.address.create(obj_in=create_address)
                order_priv.address_id = address_id
            else:
                return JSONResponse(
                    status_code=status.HTTP_405_METHOD_NOT_ALLOWED, 
                    content={
                        'msg': 'Primero debe ingresar una dirreción de pago', 
                        'type': ErrorTypeEnum.warning
                })
        
        await asyncio.gather(*stock_tasks)

        order, order_items = await asyncio.gather(
            crud_order.order.create(order_priv),
            crud_order_items.order_item.create_bulk(oi_required),
        )

        if isinstance(order_items, list):
            await crud_order_assotion.orders_asso.create_bulk(
                [{'order_id': order['id'], 'order_items_id': oiid['id']} for oiid in order_items]
            )
        else:
            await crud_order_assotion.orders_asso.create_bulk(
                {'order_id': order['id'], 'order_items_id': order_items['id']}
            )
        
    except Exception as e:
        print('exception: ', e)
        await transaction.rollback()
        return JSONResponse(
            status_code=status.HTTP_502_BAD_GATEWAY, 
            content={
                'msg': 'Ha habido un error procesando la orden, intente nuevamente más tarde', 
                'type': ErrorTypeEnum.warning
            })

    else:
        await transaction.commit()
        await cart.delete_carts()
        return {'order_id': order['id']}


@router.get(
    "/all",
    response_model=List[order_sch.OrderSch],
    response_model_exclude_none=True
)
async def get_orders(
    skip: int = 0,
    limit: int = 100,
    show_items: bool = False,
    order_by: str = 'created_at',
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "cashier_admin", "general_admin"])
) -> Any:
    orders = await crud_order.order.get_multi_order_by(
        skip=skip, 
        limit=limit,
        order_by=order_by
    )
    if not show_items:
        return orders
    else:
        orders_rp = []
        for order in orders:
            order_rp = {
                'id': order['id'],
                'address_id': order['address_id'],
                'created_at': order['created_at'],
                'payment': order['payment'],
                'order_items': []
            }
            asso_obj = await crud_order_assotion.orders_asso.get_multi_by_order(
                order['id'],
            )
            oi_ids = map(lambda order: order.get('order_items_id'), asso_obj)
            items = await crud_order_items.order_item.get_by_ids(
                oi_ids
            )
            for i in items:
                order_rp['order_items'].append({
                    'item_id': i['item_id'],
                    'quantity': i['quantity']
                })
            orders_rp.append(order_rp)

        return orders_rp


@router.get(
    "/self", 
    response_model=order_sch.OrdersPaginated,
    response_model_exclude_none=True,
)
async def get_self_orders(
    page: int = 0,
    size: int = 3,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "cashier_admin"])
):
    limit, offset = get_pagination(size, page).values()
    orders_count = await crud_order.order.get_count(current_user['id'])
    orders = await crud_order.order.get_own_multi(
        user_id=current_user['id'], limit=limit, skip=offset
    )
    return await get_pagination_data(
        {'count': orders_count, 'orders': orders}, 
        page,
        limit
    )



@router.get(
    "/{order_id}", 
    response_model=order_sch.OrderSch,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': Message,
            'description': "The obj doesn't exists"
        }        
    }
)
async def get_order_by_id(
    order_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "cashier_admin"])
) -> Any:
    order = await crud_order.order.get(
        id=order_id
    )
    if not order:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'Esta orden no existe', 
                'type': ErrorTypeEnum.warning
            })
    
    asso_obj = await crud_order_assotion.orders_asso.get_multi_by_order(
        order['id'],
    )
    oi_ids = map(lambda order: order.get('order_items_id'), asso_obj)
    items = await crud_order_items.order_item.get_by_ids(
        oi_ids
    )
    order_items = []
    for i in items:
        order_items.append({
            'id': i['id'],
            'item_id': i['item_id'],
            'quantity': i['quantity']
        })

    response = order_sch.OrderSch(**order, order_items=order_items)
    return response


@router.get(
    "/order-items/{order_id}", 
    response_model=List[order_item_sch.OrderItemSch],
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': Message,
            'description': "The obj doesn't exists"
        }  
    }
)
async def get_all_order_items(
    order_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "cashier_admin"])
) -> Any:
    """
        Get all OrderItems associated to an Order Object
    """
    order = await crud_order.order.get(
        id=order_id
    )
    if not order:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'Esta orden no existe', 
                'type': ErrorTypeEnum.warning
            })
    
    asso_obj = await crud_order_assotion.orders_asso.get_multi_by_order(
        order['id'],
    )
    oi_ids = map(lambda order: order.get('order_items_id'), asso_obj)
    items = await crud_order_items.order_item.get_by_ids(
        oi_ids
    )
    order_items = []
    for i in items:
        order_items.append(
            order_item_sch.OrderItemInDB(
            id = i['id'],
            customer_id = i['customer_id'],
            item_id = i['item_id'], 
            quantity = i['quantity']
            )
        )

    return order_items


@router.patch(
    "/payment/{order_id}",
    responses={
        status.HTTP_201_CREATED: {
            'model': order_sch.OrderSch,
            'description': "Payment data successfully updated"
        },
         status.HTTP_404_NOT_FOUND: {
            'model': Message,
            'description': "The obj doesn't exists"
        },
    }
)
async def update_order_payment(
    *,
    order_id: int,
    order_in: order_sch.OrderUpdatePayment,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "cashier_admin"])
) -> Any:
    order = await crud_order.order.get(id=order_id)
    if not order:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'Esta orden no existe', 
                'type': ErrorTypeEnum.warning
            })
    order_schema = order_sch.OrderPayment(**order)
    order = await crud_order.order.update(db_obj=order_schema, obj_in=order_in)
    return order