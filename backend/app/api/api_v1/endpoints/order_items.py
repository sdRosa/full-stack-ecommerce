from os import stat
from aioredis.commands import Redis
from fastapi import APIRouter, status, Security, HTTPException
from fastapi.responses import Response, JSONResponse
from typing import Any
import asyncio

from app.api import deps
from app.crud import crud_item, crud_order, crud_order_assotion, crud_order_items
from app.db.db_setup import db
from app.redis.connection import rd
from app.redis.item import Item
from app.schemas import order_item_sch, order_sch, user_sch, item_sch
from app.schemas.msg_sch import Message, ErrorTypeEnum


router = APIRouter()

# async def items_update(
#     item_id: int, 
#     item_rd: Dict[str, Any], 
#     item_conn: Item, 
#     oi_quantity: int,
#     rd_transaction: bool,
#     **args
# ):
#     tasks = []
#     tasks.append(
#         crud_item.item.update_in_place(
#             args['company_id'],
#             item_id,
#             args['decremented_i'])
#         )
#     if item_conn and rd_transaction:
#         tasks.append(
#             item_conn.remove_quantities(item_id, item_rd, oi_quantity)
#         )
#     return await asyncio.gather(**tasks)

@router.get(
    "/{oi_id}", 
    response_model=order_item_sch.OrderItemOrderConn,
    responses={
        status.HTTP_404_NOT_FOUND:{
            "model": Message,
            "description": "The obj doesn't exists"
        },
    }
)
async def get_order_item(
    oi_id: int,
    order_connection: bool,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "cashier_admin"])
) -> Any:
    order_item = await crud_order_items.order_item.get(oi_id)
    if not order_item:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'El item de orden no existe', 
                'type': ErrorTypeEnum.warning
            })
    if order_connection:
        oi_n_conn = await crud_order_assotion.orders_asso.get_by_oi(oi_id)
        rp = order_item_sch.OrderItemOrderConn(**order_item)
        rp.order_id = oi_n_conn['order_id']
        return rp

    return order_item_sch.OrderItemOrderConn(**order_item)


@router.patch(
    "/{oi_id}",
    responses={
        status.HTTP_201_CREATED:{
            "model": order_item_sch.OrderItemInDB,
            "description": "The obj was updated successfully"
        },
        status.HTTP_404_NOT_FOUND:{
            "model": Message,
            "description": "The obj doesn't exists"
        },
        status.HTTP_400_BAD_REQUEST:{
            "model": Message,
            "description": "The new quantity is equal to the old one"
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY:{
            "model": Message,
            "description": "The quantity on update can't be 0 or less"
        },
        status.HTTP_500_INTERNAL_SERVER_ERROR:{
            "model": Message,
            "description": "Not enough stock to complete the transaction"
        },
    }
)
async def update_order_item(
    order_id: int,
    oi_id: int,
    oi_in: order_item_sch.OrderItemUpdateQuantity,
    rp: Response,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "cashier_admin"])
) -> Any:
    if oi_in.quantity <= 0:
        return JSONResponse(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, 
            content={
                'msg': 'La cantidad no puede ser 0 o menor', 
                'type': ErrorTypeEnum.warning
            })
    # crear un join para los 3 queries
    order_asso = await crud_order_assotion.orders_asso.get_by_order_oi(order_id, oi_id)
    if not order_asso:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'Aun no existe la orden de producto para esta orden', 
                'type': ErrorTypeEnum.warning
            })

    order_item = await crud_order_items.order_item.get(oi_id)
    order = await crud_order.order.get(order_id)
    if not (order_item or order):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'Esta orden de producto u orden no existe', 
                'type': ErrorTypeEnum.warning
            })

    pg_item = await crud_item.item.get(order_item['item_id'])
    if not pg_item:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'El producto no existe actualmente', 
                'type': ErrorTypeEnum.warning
            })

    order_schema = order_sch.OrderInDB(**order)
    order_update = order_sch.OrderUpdateSubtotal(**order)
    oi_schema = order_item_sch.OrderItemInDB(**order_item)

    rd_conn = await rd.create_redis()
    item_conn = Item(
        redis_connection=rd_conn,
        item_id=oi_schema.item_id
    )
    rd_item = await item_conn.get_obj(oi_schema.item_id, force_query=True)
    tasks = []

    transaction = await db.transaction()

    try:
        if oi_in.quantity == oi_schema.quantity:
            return JSONResponse(
                status_code=status.HTTP_400_BAD_REQUEST, 
                content={
                    'msg': 'La nueva cantidad es igual a la actual', 
                    'type': ErrorTypeEnum.warning
                })

        elif oi_in.quantity > oi_schema.quantity: 
            decr_qn = oi_in.quantity - oi_schema.quantity 

            if decr_qn >= pg_item['stock_bucket']:
                new_bucket = decr_qn - pg_item['stock_bucket']
                new_kg = (decr_qn * pg_item['unit_kg']) - pg_item['stock_kg']
            else:
                new_bucket = pg_item['stock_bucket'] - decr_qn
                new_kg = pg_item['stock_kg'] - (decr_qn * pg_item['unit_kg'])

            new_stock = {
                'stock_kg': new_kg,
                'stock_bucket': new_bucket
            }
            order_update.subtotal = order_schema.subtotal + (pg_item['unit_price'] * decr_qn)
            decremented_i = item_sch.UpdateStock(**new_stock)

            if (new_bucket or new_kg) < 0:
                return JSONResponse(
                    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, 
                    content={
                        'msg': 'Stock insuficiente para incrementar cantidad', 
                        'type': ErrorTypeEnum.warning
                    })
            if rd_item:
                tasks.append(
                    crud_item.item.update_in_place(
                        oi_schema.item_id,
                        decremented_i
                    )
                )
                tasks.append(crud_order.order.update(db_obj=order_schema, obj_in=order_update))
                tasks.append(
                    item_conn.remove_quantities(
                        oi_schema.item_id, rd_item, oi_in.quantity
                    )
                )
            else:
                tasks.append(
                    crud_item.item.update_in_place(
                        oi_schema.item_id,
                        decremented_i
                    )
                )
                tasks.append(crud_order.order.update(db_obj=order_schema, obj_in=order_update))

        else:
            decr_qn = oi_schema.quantity - oi_in.quantity 
            
            new_bucket = decr_qn + pg_item['stock_bucket']
            new_kg = (decr_qn * pg_item['unit_kg']) + pg_item['stock_kg']

            new_stock = {
                'stock_kg': new_kg,
                'stock_bucket': new_bucket
            }
            order_update.subtotal = order_update.subtotal - (pg_item['unit_price'] * decr_qn)
            incremented_i = item_sch.UpdateStock(**new_stock)

            if (new_bucket or new_kg) < 0:
                return JSONResponse(
                    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, 
                    content={
                        'msg': 'Stock insuficiente para incrementar cantidad', 
                        'type': ErrorTypeEnum.warning
                    })
            if rd_item:
                tasks.append(
                    crud_item.item.update_in_place(
                        oi_schema.item_id,
                        incremented_i
                    )
                )
                tasks.append(crud_order.order.update(db_obj=order_schema, obj_in=order_update))
                tasks.append(
                    item_conn.update_item_quantity(
                        oi_schema.item_id, rd_item, oi_in.quantity
                    )
                )
            else:
                tasks.append(
                    crud_item.item.update_in_place(
                        oi_schema.item_id,
                        incremented_i
                        )
                    )
                tasks.append(crud_order.order.update(db_obj=order_schema, obj_in=order_update))

        tasks.append(crud_order_items.order_item.update(db_obj=oi_schema, obj_in=oi_in))
        await asyncio.gather(*tasks)
        oi_schema.quantity = oi_in.quantity

    except Exception as e:
        print('exception: ', e)
        await transaction.rollback()
        return JSONResponse(
            status_code=status.HTTP_502_BAD_GATEWAY, 
            content={
                'msg': 'Error actualizando cantidad de orden, intente mas tarde', 
                'type': ErrorTypeEnum.warning
            })
    else:
        await transaction.commit()
        rp.status_code = status.HTTP_200_OK
        return oi_schema

