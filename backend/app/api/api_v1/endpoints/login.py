from datetime import timedelta
from typing import Any

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordRequestForm

from app.api import deps
from app.crud import crud_user
from app.core import security
from app.core.config import settings
from app.core.security import (
    create_access_token,
    get_password_hash, 
    verify_password
)
from app.schemas import token_sch
from app.schemas.msg_sch import Message, ErrorTypeEnum
# from app.utils import (
#     generate_password_reset_token,
#     send_reset_password_email,
#     verify_password_reset_token,
# )


router = APIRouter()


@router.post(
    "/access-token", 
    response_model=token_sch.Token,
    responses={
        status.HTTP_400_BAD_REQUEST:{
            'model': Message, 
            'description': 'Inactive User or incorrect email or password'
        }
    }
)
async def login_access_token(
    form_data: OAuth2PasswordRequestForm = Depends()
) -> Any:
    """
    OAuth2 compatible token login, get an access token for future requests
    """
    user = await crud_user.user.authenticate(
        email=form_data.username, password=form_data.password
    )
    if not user:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST, 
            content={
                'msg': 'Email o contraseña incorrectos', 
                'type': ErrorTypeEnum.warning
            })
    elif not await crud_user.user.is_active(user):
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST, 
            content={
                'msg': 'Usuario inactivo', 
                'type': ErrorTypeEnum.warning
            })
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    return {
        "access_token": security.create_access_token(
            user['id'], scopes=user['scopes'], expires_delta=access_token_expires
        ),
        "token_type": "bearer",
        "user_id": user['id'],
        'username': user['name'],
        "ttl": access_token_expires
    }