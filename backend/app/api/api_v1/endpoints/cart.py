from typing import Any, List

from fastapi import APIRouter, Depends, Security, status, HTTPException
from fastapi import responses
from fastapi.responses import JSONResponse, Response
import asyncio

from aioredis import WatchVariableError
from app.api import deps
from app.crud import crud_item
from app.schemas import user_sch, item_sch, msg_sch
from app.schemas.redis import rd_cart_sch, rd_item_sch
from app.redis.cart import Cart
from app.redis.item import Item
from app.redis.connection import RedisConn


router = APIRouter()


@router.post(
    "/add/{item_id}",
    response_model=rd_cart_sch.RDCart,
    responses={
        status.HTTP_201_CREATED: {
            'model': rd_cart_sch.RDCart, 
            "description": "The obj was created successfully"
        },
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
        status.HTTP_502_BAD_GATEWAY: {
            'model': msg_sch.Message, 
            "description": "Stock quantity too low for complete the transaction"
        },
    } 
)
async def add_to_cart( 
    *,
    item_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "cashier_admin"]),
    rp: Response
) -> Any:
    open_conn = RedisConn()
    conn = await open_conn.create_redis()

    cart = Cart(
        redis_connection=conn,
        customer_id=current_user['id'], 
    )
    item_rd = Item(
        redis_connection=cart.redis_connection,
        item_id=item_id,
    )
    await cart._init()
    await item_rd._init()
 
    cart_item, item_in_rd = await asyncio.gather(
        cart.get_obj(item_id), 
        item_rd.get_obj(item_id)
    )
    if item_in_rd:
        item_in_rd = rd_item_sch.RDItem(**item_in_rd)
        while True:
            error_count = 0
            try: 
                await conn.watch(item_id)
                if item_in_rd.pg_stock > 0:
                    if cart_item:
                        cart_item = rd_cart_sch.RDCart(**cart_item)
                        new_quantity, _ = await asyncio.gather(
                            cart.update_cart_quantity(item_id),
                            item_rd.remove_quantities(item_id, item_in_rd)
                        )
                        cart_item.quantity = new_quantity
                        rp.status_code = status.HTTP_200_OK
                        return cart_item
                    else:
                        new_cart = rd_cart_sch.RDCartCreate(
                            item_id = item_id,
                            name = item_in_rd.name,
                            unit_price = str(item_in_rd.unit_price),
                            image_url = item_in_rd.image_url,
                            quantity = 1,
                        )
                        
                        await asyncio.gather(
                            cart.add(new_cart), 
                            item_rd.remove_quantities(item_id, item_in_rd)
                        )
                        rp.status_code = status.HTTP_201_CREATED
                        return await cart.get_obj(item_id)
                else:
                    await conn.unwatch()
                    return JSONResponse(
                        status_code=status.HTTP_502_BAD_GATEWAY, 
                        content={
                            'msg': 'Catidad insuficiente en stock para añadir al carrito', 
                            'type': msg_sch.ErrorTypeEnum.warning
                        })

            except WatchVariableError:
                error_count += 1
                continue 
    else:
        raw_item = await crud_item.item.get(item_id)
        item_pg = item_sch.ItemInDB(**raw_item)
        if not item_pg or item_pg.stock <= 0:
            return JSONResponse(
                status_code=status.HTTP_404_NOT_FOUND, 
                content={
                    'msg': 'Este item no esta disponible actualmente', 
                    'type': msg_sch.ErrorTypeEnum.warning
                })

        new_stock = item_pg.stock - 1
        new_item = rd_item_sch.RDItemCreate(
            item_id = item_pg.id,
            name = item_pg.name,
            unit_price = str(item_pg.unit_price),
            discount_price = str(item_pg.discount_price),
            sold = item_pg.sold,
            pg_stock = new_stock,
            image_url = item_pg.image_url,
        )
        tasks = []
        tasks.append(item_rd.add(new_item))

        if cart_item:
            cart_item = rd_cart_sch.RDCart(**cart_item)
            new_quantity = await cart.update_cart_quantity(item_id)
            cart_item.quantity = new_quantity
            rp.status_code = status.HTTP_200_OK
            return cart_item

        new_cart = rd_cart_sch.RDCartCreate(
            item_id = item_pg.id,
            name = item_pg.name,
            unit_price = str(item_pg.unit_price),
            image_url = item_pg.image_url,
            quantity = 1,
        )
        tasks.append(cart.add(new_cart))
        await asyncio.gather(*tasks)

        rp.status_code = status.HTTP_201_CREATED
        return await cart.get_obj(item_id)
                    

@router.delete(
    "/remove-item/{item_id}",
    responses={
        status.HTTP_200_OK: {
            'model': msg_sch.Message, 
            "description": "The obj was deleted successfully"
        },
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
    } 
)
async def remove_item_cart(
    *,
    item_id: int,
    cart: Cart = Depends(deps.init_redis_cart_conn)
) -> Any:
    """
        Remove an item of a cart (And quantities of that one).
    """
    item_rd = Item(
        redis_connection=cart.redis_connection,
        item_id=item_id
    )

    await item_rd._init()

    raw_item_cart = await cart.get_obj(item_id)
    raw_item = await item_rd.get_obj(item_id)
    # testear por si el ttl de item termina primero 
    if raw_item_cart and raw_item:
        item_in_cart = rd_cart_sch.RDCart(**raw_item_cart)
        item_in = rd_item_sch.RDItem(**raw_item)

        del_item_cart = cart.remove(item_id)
        upgrade_quantity = item_rd.update_item_quantity(
            item_id,
            item_in, 
            quantity=item_in_cart.quantity,
        )
        is_item_del, _ = await asyncio.gather(del_item_cart, upgrade_quantity)
        if is_item_del:
            return JSONResponse(
                status_code=status.HTTP_200_OK, 
                content={
                    'msg': 'El item fue eliminado existosamente', 
                    'type': msg_sch.ErrorTypeEnum.success
                })
    else:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'Este item no existe en el carrito', 
                'type': msg_sch.ErrorTypeEnum.warning
            })
 
@router.patch(
    "/remove-one/{item_id}",
    response_model=rd_cart_sch.RDCartLessQuantity,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
        status.HTTP_502_BAD_GATEWAY: {
            'model': msg_sch.Message, 
            "description": "The obj could exists but stock is negative for that one"
        }
    } 
)
async def remove_quantity_item_cart(
    *,
    item_id: int,
    cart: Cart = Depends(deps.init_redis_cart_conn),
) -> Any:
    """
        Remove an item's quantity of a cart.
    """
    item_rd = Item(
        redis_connection=cart.redis_connection,
        item_id=item_id
    )
    await item_rd._init()

    item_in_cart = await cart.get_obj(item_id)
    if not item_rd.obj_exists:
        return JSONResponse(
            status_code=status.HTTP_502_BAD_GATEWAY, 
            content={
                'msg': 'Este item no está disponible actualmente, intente más tarde', 
                'type': msg_sch.ErrorTypeEnum.warning
            })

    if item_in_cart:
        raw_item = await item_rd.get_obj(item_id)
        item_in_rd = rd_item_sch.RDItem(**raw_item)
        less_quantity = cart.remove_quantities(item_id, item_in_cart)
        upgrade_stock = item_rd.update_item_quantity(
            item_id,
            item_in_rd
        )
        item_del, _ = await asyncio.gather(less_quantity, upgrade_stock)
        return item_del
    else:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'Este item no existe en el carrito', 
                'type': msg_sch.ErrorTypeEnum.warning
            })


@router.get("/", response_model=rd_cart_sch.RDCartWithCount)
async def get_items_cart(
    *,
    cart: Cart = Depends(deps.init_redis_cart_conn),
) -> Any:
    items = await cart.get_item_dicts()
    if items:
        total = await cart.subtotal_cost()
        return {'total': total, 'products': items}
    else:
        return {'total': 0, 'products': []}

# @router.get(
#     "/bulk-items",
#     response_model=List[rd_cart_sch.RDCart],
#     responses={
#         status.HTTP_404_NOT_FOUND: {
#             'model': msg_sch.Message, 
#             "description": "The obj doesn't exists"
#         },
#     }
# )
# async def get_items_for_bulk(
#     *,
#     cart: Cart = Depends(deps.init_redis_cart_conn)
# ) -> Any:

#     items = await cart.get_item_dicts_bulk()
#     if items:
#         return items
#     else:
#         return JSONResponse(
#             status_code=status.HTTP_404_NOT_FOUND, 
#             content={
#                 'msg': 'No hay ningún carrito creado ahora mismo', 
#                 'type': msg_sch.ErrorTypeEnum.warning
#             })


@router.get(
    "/count",
    response_model=rd_cart_sch.RDCartItemsCount,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
    }
)
async def get_items_type_counts(
    *,
    cart: Cart = Depends(deps.init_redis_cart_conn),
) -> Any:
    """
        Returns a count of all types of items in cart.
    """
    items_count = await cart.count()
    if items_count > 0:
        return {'items_count': items_count}
    else:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'No hay ningún carrito creado ahora mismo', 
                'type': msg_sch.ErrorTypeEnum.warning
            })


@router.get(
    "/count-all",
    response_model=rd_cart_sch.RDCartItemsCount,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
    }
)
async def get_items_counts(
    *,
    cart: Cart = Depends(deps.init_redis_cart_conn),
) -> Any:
    """
        Returns a count of all items in cart.
    """
    cart_exists = cart.obj_exists_in
    if cart_exists:
        items_quantities = await cart.quantity()
        return {'items_count': items_quantities}
    else:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'No hay ningún carrito creado ahora mismo', 
                'type': msg_sch.ErrorTypeEnum.warning
            })


@router.get(
    "/subtotal",
    response_model=rd_cart_sch.RDCartSubtotal,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
    }
)
async def get_subtotal(
    cart: Cart = Depends(deps.init_redis_cart_conn),
) -> Any:
    """
        Returns a subtotal of all items.
    """
    cart_exists = cart.obj_exists_in
    if cart_exists:
        subtotal = await cart.subtotal_cost()
        if subtotal:
            return {'subtotal': subtotal}
        return {'subtotal': 0}
    else:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'No hay ningún carrito creado ahora mismo', 
                'type': msg_sch.ErrorTypeEnum.warning
            })