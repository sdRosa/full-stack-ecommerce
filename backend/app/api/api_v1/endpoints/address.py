from asyncpg.exceptions import ForeignKeyViolationError
from typing import Any, List

from fastapi import (
    APIRouter, 
    Security, 
    status, 
    Response)
from fastapi.responses import JSONResponse

from app.api import deps
from app.crud import (
    crud_address, 
    crud_user, 
    crud_order)
from app.schemas import (
    address_sch, 
    msg_sch, 
    user_sch)


router = APIRouter()
 

@router.post(
    "/create",
    response_model_exclude_none=True,
    response_model=address_sch.AddressSch,
    responses={
        status.HTTP_201_CREATED: {
            'model': address_sch.AddressSch, 
            "description": "The obj was overwrited/updated successfully"
        },
        status.HTTP_400_BAD_REQUEST: {
            'model': msg_sch.Message, 
            "description": "The obj exists, but overwrite is set to False"
        },
        status.HTTP_401_UNAUTHORIZED: {
            'model': msg_sch.Message, 
            "description": "The current user tried to create an address for another user"
        },
    } 
)
async def create_address(
    *,
    address_in: address_sch.AddressCreatePriv,
    set_default: bool,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"]),
    rp: Response
) -> Any:
    if address_in.user_id != current_user['id']:
        return JSONResponse(
            status_code=status.HTTP_401_UNAUTHORIZED, 
            content={
                'msg': f"The current user and address's owner should match", 
                'type': msg_sch.ErrorTypeEnum.danger,
            })

    address_id = await crud_address.address.create(
        obj_in=address_in, 
    )
    if set_default:
        own_user = user_sch.UserSch(**current_user)
        update_user = user_sch.UserUpdate(
            default_address=set_default, address_id=address_id)

        await crud_user.user.update(
            db_obj=own_user, obj_in=update_user
        )
    # address_rp = address_sch.AddressSch(**address_in.dict(), id=address_id)
    rp.status_code = status.HTTP_200_OK
    return {
        'id': address_id,
        **address_in.dict()
    }


@router.get(
    "/all", 
    response_model=List[address_sch.AddressInDB]
)
async def get_address(
    skip: int = 0,
    limit: int = 100,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me"])
) -> Any:
    address = await crud_address.address.get_multi(
        skip=skip, limit=limit
        )
    return address


@router.get(
    "/self", 
    response_model=List[address_sch.AddressInDB]
)
async def get_self_addresses(
    skip: int = 0,
    limit: int = 2,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me"])
):
    addresses = await crud_address.address.get_own_addresses(
        current_user['id'], skip, limit
    )
    orders_filter = await crud_order.order.get_own_multi(user_id=current_user['id'])
    not_editable = {o['address_id'] for o in orders_filter}

    return [a for a in addresses if a['id'] not in not_editable]


@router.get(
    "/latest",
    response_model=address_sch.AddressInDB,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
    }
)
async def get_latest_address(
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me"])
) -> Any:
    latest_address = await crud_address.address.get_latest(current_user['id'])
    if not latest_address:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'There is no address created yet', 
                'type': msg_sch.ErrorTypeEnum.warning
            })
    return address_sch.AddressInDB(**latest_address)
 

@router.get(
    "/{address_id}", 
    response_model=address_sch.AddressSch,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
    }
)
async def get_address_by_id(
    address_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me"])
) -> Any:
    address = await crud_address.address.get(id=address_id)
    if not address:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': f'La dirección con el ID "{address_id}" no existe', 
                'type': msg_sch.ErrorTypeEnum.warning
            })
    response = address_sch.AddressSch(**address)
    return response
    

@router.patch(
    "/{address_id}",
    response_model=address_sch.AddressSch,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
        status.HTTP_401_UNAUTHORIZED: {
            'model': msg_sch.Message, 
            "description": "Can't update if current user doesn't are the owner"
        },
    }
)
async def update_address(
    *,
    address_id: int,
    address_in: address_sch.AddressUpdate,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"])
) -> Any:
    address = await crud_address.address.get(id=address_id)
    if not address:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': f'La dirección con el ID "{address_id}" no existe', 
                'type': msg_sch.ErrorTypeEnum.warning
            })
    if current_user['id'] != address['user_id']:
        return JSONResponse(
            status_code=status.HTTP_401_UNAUTHORIZED, 
            content={
                'msg': f'Unauthorized to update address of another user', 
                'type': msg_sch.ErrorTypeEnum.warning
            })
    address_schema = address_sch.AddressSch(**address)
    address = await crud_address.address.update(db_obj=address_schema, obj_in=address_in)
    return address_sch.AddressSch(**address)


@router.delete(
    "/{address_id}",
    response_model=msg_sch.Message,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
        status.HTTP_502_BAD_GATEWAY: {
            'model': msg_sch.Message, 
            "description": "The obj can't be deleted bc exists an Order related to this AddressSch"
        },
    }
)
async def delete_address(
    *,
    address_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"])
) -> Any:
    address = await crud_address.address.get(id=address_id)
    if not address:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': f'La dirección con el ID "{address_id}" no existe', 
                'type': msg_sch.ErrorTypeEnum.warning
            })
    try:
        await crud_address.address.remove(id=address_id)
    except ForeignKeyViolationError as e:
        return JSONResponse(
            status_code=status.HTTP_502_BAD_GATEWAY, 
            content={
                'msg': f'La dirección no puede ser eliminada porque aun es referida en una orden', 
                'type': msg_sch.ErrorTypeEnum.danger,
            })
    
    return msg_sch.Message(
        msg=f"Dirección del cliente '{address['customer_id']}' eliminada", 
        type="success")


@router.delete(
    "/self/{address_id}",
    response_model=msg_sch.Message,
    responses={
        status.HTTP_404_NOT_FOUND: {
            'model': msg_sch.Message, 
            "description": "The obj doesn't exists"
        },
        status.HTTP_401_UNAUTHORIZED: {
            'model': msg_sch.Message, 
            "description": "The obj can't be deleted bc the actual user doesn't own this address"
        },
        status.HTTP_502_BAD_GATEWAY: {
            'model': msg_sch.Message, 
            "description": "The obj can't be deleted bc exists an Order related to this AddressSch"
        },
    }
)
async def delete_own_address(
    *,
    address_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "franchise_admin"])
) -> Any:
    address = await crud_address.address.get(id=address_id)
    if not address:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': f'La dirección con el ID "{address_id}" no existe', 
                'type': msg_sch.ErrorTypeEnum.warning
            })
    if address['user_id'] == current_user['id']:
        try:
            await crud_address.address.remove(id=address_id)
        except ForeignKeyViolationError as e:
            return JSONResponse(
                status_code=status.HTTP_502_BAD_GATEWAY, 
                content={
                    'msg': f'La dirección no puede ser eliminada porque aun es referida en una orden', 
                    'type': msg_sch.ErrorTypeEnum.danger,
                })
        
        return msg_sch.Message(
            msg=f"User's address '{address['user_id']}' deleted successfully", 
            type="success")
    else:
        return JSONResponse(
            status_code=status.HTTP_401_UNAUTHORIZED, 
            content={
                'msg': f"Actual user doesn't own this address. Can't be deleted", 
                'type': msg_sch.ErrorTypeEnum.danger,
            })