from typing import Any, List

from fastapi import APIRouter, Security, status
from fastapi.responses import JSONResponse, Response
from sqlalchemy.sql.functions import user

from app.crud import crud_user, crud_address
from app.schemas import user_sch
from app.schemas.msg_sch import Message, ErrorTypeEnum
from app.api import deps
# from app.core.config import settings
# from app.utils import send_new_account_email

router = APIRouter()

 
@router.post(
    "/register", 
    responses={
        status.HTTP_201_CREATED: {
            'model': Message,
            'description': "The was created successfully"
        },
        status.HTTP_404_NOT_FOUND: {
            'model': Message,
            'description': "The obj doesn't exists"
        },
        status.HTTP_500_INTERNAL_SERVER_ERROR: {
            'model': Message,
            'description': "Error handling user creation at db level"
        }
    }
)
async def create_user(
    *,
    user_in: user_sch.UserCreate,
) -> Any:
    user = await crud_user.user.get_by_email(email=user_in.email)
    if user:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST, 
            content={
                'msg': f"El correo '{user_in.email}' ya está siendo utilizado", 
                'type': ErrorTypeEnum.danger
            })
    new_user = user_sch.UserCreatePriv(**user_in.dict())
    user_id = await crud_user.user.create(obj_in=new_user)
    # if settings.EMAILS_ENABLED and user_in.email:
    #     send_new_account_email(
    #         email_to=user_in.email, username=user_in.email, password=user_in.password
    #     )
    if user_id:
        return JSONResponse(
            status_code=status.HTTP_201_CREATED, 
            content={
                'msg': f"Usuario creado exitosamente! Autenticar para ingresar", 
                'type': ErrorTypeEnum.success
            })
    else:
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, 
            content={
                'msg': f"El servicio no está disponible actualmente. Intente mas tarde", 
                'type': ErrorTypeEnum.warning
            })


@router.get("/all", response_model=List[user_sch.UserSch])
async def get_users(
    skip: int = 0,
    limit: int = 100,
    is_active: bool = True,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "general_admin"])
) -> Any:
    users = await crud_user.user.get_multi(
        skip=skip, limit=limit, is_active=is_active
    )
    return users


@router.get(
    "/self",
    response_model=user_sch.UserSch
)
async def get_me(
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "general_admin"])
) -> Any:
    return user_sch.UserSch(**current_user)


@router.patch(
    "/default-address",
    response_model=user_sch.DefaultAddress,
    responses={
        status.HTTP_404_NOT_FOUND:{
            'model': Message,
            'description': "The obj doesn't exists"
        }
    }
)
async def update_default_address(
    set_default_address: bool,
    address_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me"])
):
    address_exists = await crud_address.address.get(address_id)
    user_schema = user_sch.UserSch(**current_user)
    if not address_exists:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': "The address doesn't exists", 
                'type': ErrorTypeEnum.warning
            })

    if set_default_address:
        update_user = user_sch.UserUpdate(
            default_address=True, address_id=address_id)

        updated = await crud_user.user.update(
            db_obj=user_schema, obj_in=update_user
        )

    else:
        update_user = user_sch.UserUpdate(
            default_address=False, address_id=None)

        updated = await crud_user.user.update(
            db_obj=user_schema, obj_in=update_user)
    
    return {
        'default_address': updated['default_address'],
        'address_id': updated['address_id']
    }


@router.get(
    "/{user_id}", 
    response_model=user_sch.UserInDB,
    responses={
        status.HTTP_404_NOT_FOUND:{
            'model': Message,
            'descriptiop': "The obj doesn't exists"
        }
    }
)
async def get_user_by_id(
    user_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "general_admin"])
) -> Any:
    if user_id == current_user['id']:
        return user_sch.UserSch(**current_user)
        
    user = await crud_user.user.get(id=user_id)
    if not user:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': 'Este usuario no existe', 
                'type': ErrorTypeEnum.warning
            })
    response = user_sch.UserSchInDB(**user)
    return response
    

@router.patch(
    "/{user_id}",
    responses={
        status.HTTP_201_CREATED:{
            'model': user_sch.UserSch,
            'description': "The obj doesn't exists"
        },
        status.HTTP_404_NOT_FOUND:{
            'model': Message,
            'description': "The obj doesn't exists"
        }
    }
)
async def update_user(
    *,
    user_id: int,
    user_in: user_sch.UserUpdate,
    rp: Response,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "general_admin"])
) -> Any:
    user = await crud_user.user.get(id=user_id)
    user_schema = user_sch.UserSch(**user)
    if not user:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND, 
            content={
                'msg': "El usuario no existe en el sistema", 
                'type': ErrorTypeEnum.warning
            })
    user = await crud_user.user.update(db_obj=user_schema, obj_in=user_in)
    rp.status_code = status.HTTP_201_CREATED
    return user_in.dict(exclude_none=True)


@router.delete(
    "/{user_id}",
    response_model=Message,
    responses={
        status.HTTP_404_NOT_FOUND:{
            'model': Message,
            'description': "The obj doesn't exists"
        }
    }
)
async def delete_user(
    *,
    user_id: int,
    current_user: user_sch.UserSch = Security(deps.get_current_active_user, scopes=["me", "general_admin"])
) -> Any:
    user = await crud_user.user.get(id=user_id)
    if not user:
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST, 
            content={
                'msg': "El no existe en el sistema", 
                'type': ErrorTypeEnum.danger
            })
    # if not crud_user.user.is_superuser(current_user) or (user.id != current_user.id):
    #     raise HTTPException(status_code=400, detail="Permisos insuficientes")
    user = await crud_user.user.remove(id=user_id)
    del_msg = {
        'msg': "El usuario ha sido eliminado exitosamente", 
        'type': ErrorTypeEnum.success
    }
    return Message(**del_msg)