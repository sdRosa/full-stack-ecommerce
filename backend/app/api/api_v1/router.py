from fastapi import APIRouter

from app.api.api_v1.endpoints import (
    address,
    categories,
    cart,
    items, 
    login,
    order,
    order_items,
    users, 
)

api_router = APIRouter()
api_router.include_router(address.router, prefix="/address", tags=["address"])
api_router.include_router(categories.router, prefix="/categories", tags=["categories"])
api_router.include_router(cart.router, prefix="/carts", tags=["carts"])
api_router.include_router(items.router, prefix="/items", tags=["items"])
api_router.include_router(login.router, prefix="/login", tags=["login"])
api_router.include_router(order.router, prefix="/order", tags=["order"])
api_router.include_router(order_items.router, prefix="/order-items", tags=["order-items"])
api_router.include_router(users.router, prefix="/users", tags=["users"])



