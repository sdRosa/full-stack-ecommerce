from typing import Any

from fastapi import Depends, HTTPException, status, Security
from fastapi.security import (
    OAuth2PasswordBearer, 
    SecurityScopes,
    )
from jose import jwt
from pydantic import ValidationError

from app.crud import crud_user
from app.core.config import settings
from app.core import security
from app.redis.cart import Cart
from app.redis.item import Item 
from app.redis.connection import RedisConn
from app.schemas import token_sch, user_sch


reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl=f"{settings.API_V1_STR}/login/access-token",
    scopes={"me": "Read information about the current user.", 
            "cashier_admin": "Only make orders.",
            "franchise_admin": "Products and categories CRUD and Cashier permissions.",
            "general_admin": "Cashiers CRUD and Franchise permisions."}
)

async def get_current_user(
    security_scopes: SecurityScopes, token: str = Depends(reusable_oauth2)
) -> user_sch.UserSch:
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = f"Bearer"
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Error validando las credenciales.",
        headers={"WWW-Authenticate": authenticate_value},
    )
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
        )
        token_data = token_sch.TokenPayload(**payload)
        user_id = int(token_data.sub)
        if not user_id:
            raise credentials_exception
    except jwt.ExpiredSignatureError:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Sesion expirada, inicie sesion nuevamente.",
        )

    except (jwt.JWTError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Error validando las credenciales.",
        )
    user = await crud_user.user.get(id=user_id)
    if not user:
        raise credentials_exception
    for scope in security_scopes.scopes:
        if scope not in token_data.scopes:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Permisos insuficientes.",
                headers={"WWW-Authenticate": authenticate_value},
            )
    return user


async def get_current_active_user(
    current_user: user_sch.UserSch = Security(get_current_user, scopes=["me"])
) -> user_sch.UserSch:
    if not await crud_user.user.is_active(current_user):
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


async def init_redis_cart_conn(
    current_user: user_sch.UserSch = Security(get_current_active_user, scopes=['me', 'cashier_admin'])
) -> Cart:
    open_conn = RedisConn()
    conn = await open_conn.create_redis()
    cart = Cart(
        redis_connection=conn,
        customer_id=current_user['id'], 
    )
    await cart._init()

    return cart


async def init_redis_item_conn(
    item_id: int,
) -> Item:
    open_conn = RedisConn()
    conn = await open_conn.create_redis()
    item_rd = Item(
        redis_connection=conn,
        item_id=item_id
    )
    await item_rd._init()

    return item_rd

# async def get_rd_item(current_user: Callable, )