from typing import List
from pydantic import AnyHttpUrl, BaseSettings, EmailStr, HttpUrl, PostgresDsn

import secrets
import os, sys

from dotenv import load_dotenv


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
load_dotenv(os.path.join(BASE_DIR, ".env"))
sys.path.append(BASE_DIR)

class Settings(BaseSettings):
    DOMAIN_HOST: str
    DOMAIN_PORT: str
    API_V1_STR: str = "/api/v1"
    PROJECT_NAME: str
    SECRET_KEY: str = secrets.token_urlsafe(32)
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []
    DATABASE_URL: PostgresDsn

    REDIS_HOST: str
    REDIS_PORT: int
    REDIS_DB: int

    ALGORITHM: str
    ACCESS_TOKEN_EXPIRE_MINUTES: int

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'
        

settings = Settings(_env_file='app.env', _env_file_encoding='utf-8')