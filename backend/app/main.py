from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from app.db.db_setup import db
from app.api.api_v1.router import api_router
from app.core.config import settings

import uvicorn


api_v1 = settings.API_V1_STR

app = FastAPI(
    title=settings.PROJECT_NAME, openapi_url=f"{api_v1}/openapi.json"
)
app.mount('/static', StaticFiles(directory='static'), name='static')

if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

@app.on_event("startup")
async def startup():
    await db.connect()


@app.on_event("shutdown")
async def shutdown():
    await db.disconnect()


app.include_router(api_router, prefix=api_v1)

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
