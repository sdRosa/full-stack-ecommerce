"""user: default_address

Revision ID: 641520f6d870
Revises: 24d8d1cf1d83
Create Date: 2021-06-13 10:35:09.758592

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '641520f6d870'
down_revision = '24d8d1cf1d83'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('default_address', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user', 'default_address')
    # ### end Alembic commands ###
