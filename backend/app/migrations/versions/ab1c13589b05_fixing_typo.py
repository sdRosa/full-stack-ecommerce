"""fixing typo

Revision ID: ab1c13589b05
Revises: c8478f0214d0
Create Date: 2021-07-05 09:33:45.684636

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ab1c13589b05'
down_revision = 'c8478f0214d0'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('address', sa.Column('apartment_address', sa.String(length=100), nullable=True))
    op.drop_column('address', 'aparment_address')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('address', sa.Column('aparment_address', sa.VARCHAR(length=100), autoincrement=False, nullable=True))
    op.drop_column('address', 'apartment_address')
    # ### end Alembic commands ###
