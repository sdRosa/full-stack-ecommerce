"""empty message

Revision ID: c8478f0214d0
Revises: 641520f6d870
Create Date: 2021-06-30 22:26:27.782329

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c8478f0214d0'
down_revision = '641520f6d870'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('address', 'default_address')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('address', sa.Column('default_address', sa.BOOLEAN(), autoincrement=False, nullable=True))
    # ### end Alembic commands ###
