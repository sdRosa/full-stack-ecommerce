from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import as_declarative, declared_attr

import databases

from app.core.config import settings
from typing import Any


SQLALCHEMY_DATABASE_URL = settings.DATABASE_URL

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
db = databases.Database(SQLALCHEMY_DATABASE_URL)


@as_declarative()
class Base:
    id: Any
    __name__: str
    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()
