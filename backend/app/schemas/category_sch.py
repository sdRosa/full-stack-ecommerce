from app.schemas import msg_sch

from typing import Optional
from datetime import datetime
from pydantic import BaseModel


class CategoryBase(BaseModel):
    name: Optional[str] = None
    is_active: Optional[bool] = None
 

class CategoryBaseRequired(BaseModel):
    name: str
    is_active: bool


class CategoryCreate(CategoryBaseRequired):
    pass


class CategoryUpdate(CategoryBase):
    pass
 

class CategoryInDBBase(CategoryBaseRequired):
    id: int

    class Config:
        orm_mode = True
 

class CategorySch(CategoryInDBBase):
    pass


class CategoryNames(CategoryBase):
    id: int


class CategoryInDB(CategoryInDBBase):
    created_at: datetime
