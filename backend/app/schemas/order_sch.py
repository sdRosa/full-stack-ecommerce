from app.schemas.order_item_sch import OrderItemSchPub

from datetime import datetime
from typing import Optional, List
from pydantic import BaseModel, condecimal


class OrderBase(BaseModel):
    user_id: Optional[int] = None
    address_id: Optional[int] = None # address_id 
    payment: Optional[int] = None # smallint
 

class OrderBaseRequired(BaseModel):
    payment: int


class OrderCreatePriv(OrderBaseRequired):
    address_id: Optional[int] = None 
    user_id: int
    subtotal: condecimal(decimal_places=2)


class OrderCreatePub(OrderBaseRequired):
    address_id: Optional[int] = None 
    user_id: int


class OrderUpdateSubtotal(OrderBase):
    subtotal: condecimal(decimal_places=2)


class OrderUpdate(OrderBase):
    # items: List[OrderItem]
    pass


class OrderUpdatePayment(BaseModel):
    payment: Optional[int] = None # smallint
 

class OrderInDBBase(OrderBaseRequired):
    id: int
    created_at: datetime
    subtotal: condecimal(decimal_places=2)
    address_id: Optional[int] = None  
    # items: List[OrderItem]

    class Config:
        orm_mode = True


class OrderPayment(OrderInDBBase):
    pass


class OrderSch(OrderInDBBase):
    order_items: Optional[List[OrderItemSchPub]] = None


class OrderCount(BaseModel):
    count: int
    orders: List[OrderSch]


class OrdersPaginated(BaseModel):
    total_orders: int
    orders: List[OrderSch]
    total_pages: int 
    current_page: int


class OrderId(BaseModel):
    order_id: int 


class OrderInDB(OrderInDBBase):
    user_id: int
