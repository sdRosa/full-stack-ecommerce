from app.schemas.order_item_sch import OrderItemSch
from app.schemas.order_sch import OrderSch

from pydantic import BaseModel
from typing import Optional, List


class OrderAssoBase(BaseModel):
    order_id: Optional[int] = None
    order_items_id: Optional[int] = None
 

class OrderAssoBaseRequired(BaseModel):
    order_id: int
    order_items_id: int


class OrderAssoCreate(OrderAssoBaseRequired):
    pass


class OrderAssoUpdate(OrderAssoBase):
    pass


class OrderAssoInDBBase(OrderAssoBaseRequired):
    id: int

    class Config:
        orm_mode = True


class OrderAssoSch(OrderAssoInDBBase):
    pass


class OrderAssoInDB(OrderAssoInDBBase):
    pass


class FullOrder(BaseModel):
    order: OrderSch
    order_items: List[OrderAssoInDB]
