from typing import Optional, List, Union

from pydantic import BaseModel, EmailStr, validator


class UserBase(BaseModel):
    name: Optional[str] = None
    lastname: Optional[str] = None
    scopes: Optional[List[str]] = None
    is_active: Optional[bool] = None
    address_id: Optional[int] = None 
    default_address: Optional[bool] = None


class UserBaseRequired(BaseModel):
    name: str
    lastname: Optional[str] = None
    scopes: List[str] = ['me']
    is_active: bool = True
    address_id: Optional[int] = None 
    default_address: bool = False


class UserCreate(UserBaseRequired):
    email: EmailStr
    password: str
    password2: str

    @validator('password2')
    def password_match(cls, v, values, **kwargs):
        if v != values['password']:
            raise ValueError('Las contraseñas no coinciden')
        return v

class UserCreatePriv(UserBaseRequired):
    email: EmailStr
    password: str


class UserUpdate(UserBase):
    address_id: Union[int, None]
    password: Optional[str] = None


class DefaultAddress(BaseModel):
    default_address: bool 
    address_id: Optional[int] = None


class UserInDBBase(UserBase):
    address_id: Optional[int] = None 
    id: Optional[int] = None

    class Config:
        orm_mode = True


class UserSch(UserInDBBase):
    email: EmailStr



class UserInDB(UserInDBBase):
    email: EmailStr
    # password: str
