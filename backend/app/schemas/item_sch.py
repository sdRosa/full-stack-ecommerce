from typing import Optional
from datetime import datetime
from pydantic import BaseModel, condecimal
from typing import List



class ItemBase(BaseModel):
    name: Optional[str] = None
    availability: Optional[bool] = None
    is_active: Optional[bool] = None
    featured: Optional[bool] = None
    image_url: Optional[str] = None
    stock: Optional[int] = None
    unit_price: Optional[condecimal(decimal_places=2)] = None
    discount_price: Optional[condecimal(decimal_places=2)] = None
    category_id: Optional[int] = None
 
 
class ItemBaseRequired(BaseModel):
    name: str
    availability: bool = True
    is_active: bool = True
    stock: int
    featured: bool = False
    image_url: str
    unit_price: condecimal(decimal_places=2)
    discount_price: Optional[condecimal(decimal_places=2)] = None
    category_id: int


class ItemCreate(ItemBaseRequired):
    description: str
    stock: int = 1
    sold: int = 0
    # @classmethod
    # def __get_validators__(cls):
    #     yield cls.validate_to_json

    # @classmethod
    # def validate_to_json(cls, value):
    #     if isinstance(value, str):
    #         return cls(**json.loads(value))
    #     return value


class ItemUpdate(ItemBase):
    description: Optional[str] = None


class UpdateStock(BaseModel):
    stock: condecimal(decimal_places=2)


class ItemInDBBase(ItemBaseRequired):
    sold: int
    id: int
 
    class Config:
        orm_mode = True


class ItemSch(ItemInDBBase):
    pass


class ItemsByCat(BaseModel):
    cat_name: str
    items: List[ItemSch]


class ItemCount(BaseModel):
    count: int
    items: List[ItemSch]


class ItemsInShop(BaseModel):
    total_items: int
    items: List[ItemSch]
    total_pages: int 
    current_page: int


class ItemInDB(ItemInDBBase):
    sold: int
    timestamp: datetime
    stock: int
    description: str 