from datetime import timedelta
from typing import Optional, List

from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str
    user_id: int
    username: str
    ttl: timedelta


class TokenPayload(BaseModel):
    sub: Optional[int] = None
    scopes: Optional[List[str]] = []
