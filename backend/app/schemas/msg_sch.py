from enum import Enum
from typing import List

from pydantic import BaseModel


class ErrorTypeEnum(str, Enum):
    success = 'success'
    warning = 'warning'
    danger = 'danger'
    info = 'info'


class Message(BaseModel):
    msg: str 
    type: ErrorTypeEnum


class MessageRelatedObj(Message):
    related_ids: List[int]