from typing import Optional
from pydantic import BaseModel


class OrderItemBase(BaseModel):
    user_id: Optional[int] = None
    item_id: Optional[int] = None
    quantity: Optional[int] = None
 

class OrderItemBaseRequired(BaseModel):
    user_id: int
    item_id: int
    quantity: int


class OrderItemCreatePriv(OrderItemBaseRequired):
    user_id: int


class OrderItemCreatePub(OrderItemBaseRequired):
    pass


class OrderItemUpdate(BaseModel):
    pass


class OrderItemUpdateQuantity(BaseModel):
    quantity: int


class OrderItemInDBBase(OrderItemBaseRequired):
    id: int

    class Config:
        orm_mode = True


class OrderItemSchPub(OrderItemBase):
    pass


class OrderItemSch(OrderItemInDBBase):
    pass

class OrderItemOrderConn(OrderItemInDBBase):
    order_id: Optional[int] = None


class OrderItemInDB(OrderItemInDBBase):
    user_id: int
