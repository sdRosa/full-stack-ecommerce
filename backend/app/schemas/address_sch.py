from enum import Enum
from typing import Optional

from pydantic import BaseModel, validator


class ProvinciesEnum(str, Enum):
    BA = 'BA'
    CO = 'CO'
    MZ = 'MZ'
    MI = 'MI'
    SA = 'SA'
    TU = 'TU'
    CA = 'CA'
    CH = 'CH'
    CT = 'CT'
    CR = 'CR'
    ER = 'ER'
    FO = 'FO'
    JY = 'JY'
    NE = 'NE'
    RN = 'RN'
    SJ = 'SJ'
    SL = 'SL'
    SC = 'SC'
    TF = 'TF'
    SE = 'SE'


def max_chr_len(value: str, max: int) -> str:
    if len(value) > max:
        return False
    return True

 
class AddressBase(BaseModel):
    street_address: Optional[str] = None
    apartment_address: Optional[str] = None
    province: Optional[ProvinciesEnum] = None
    zip: Optional[str] = None
    payment_note: Optional[str] = None
 

class AddressBaseRequired(BaseModel):
    street_address: str
    apartment_address: str
    province: ProvinciesEnum
    zip: str
    payment_note: Optional[str] = None

    @validator('street_address', 'apartment_address')
    def max_addresses_len(cls, v):
        max_len = 100
        if not max_chr_len(v, max_len):
            raise ValueError(f'No puede ser mayor a {max_len} caracteres')
        return v

    @validator('zip')
    def total_zip_len(cls, v):
        max_len = 4
        if len(v) > max_len or not len(v) == max_len:
            raise ValueError(f'No puede ser mayor a {max_len} caracteres o menor')
        return v

    @validator('province')
    def total_province_len(cls, v):
        max_len = 2
        if len(v) > max_len or not len(v) == max_len:
            raise ValueError(f'No puede ser mayor a {max_len} caracteres o menor')
        return v

    @validator('payment_note')
    def max_note_len(cls, v):
        max_len = 200
        if v is None:
            return v
        if not max_chr_len(v, max_len):
            raise ValueError(f'No puede ser mayor a {max_len} caracteres')
        return v


class AddressCreatePriv(AddressBaseRequired):
    user_id: int


class AddressCreatePub(AddressBaseRequired):
    pass


class AddressUpdate(AddressBase):
    pass


class AddressInDBBase(AddressBaseRequired):
    id: int

    class Config:
        orm_mode = True


class AddressSch(AddressInDBBase):
    pass


class AddressInDB(AddressInDBBase):
    user_id: int
