from typing import Union

from pydantic import BaseModel, condecimal

 
class BaseRDItem(BaseModel):
    item_id: int
    name: str
    unit_price: Union[str, condecimal(decimal_places=2)]
    discount_price: Union[str, condecimal(decimal_places=2)]
    sold: int
    pg_stock: int
    image_url: str
    extra_data_dict: dict = {}

class RDItemCreate(BaseRDItem):
    pass

class RDItem(BaseRDItem):
    pass