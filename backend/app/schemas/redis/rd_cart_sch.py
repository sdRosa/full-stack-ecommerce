from decimal import Decimal
from typing import Union, Optional, List
from pydantic import BaseModel, condecimal


class BaseRDCartRequired(BaseModel):
    item_id: int
    name: str
    unit_price: Union[str, condecimal(decimal_places=2)]
    image_url: str
    quantity: int
    extra_data_dict: dict = {}


# class BaseRDCart(BaseModel):
#     item_id: Optional[int]
#     name: Optional[str] = None
#     unit_price: Optional[Union[str, condecimal(decimal_places=2)]] = None
#     unit_kg: Optional[Union[str, condecimal(decimal_places=2)]] = None
#     image_url: Optional[str] = None
#     quantity: Optional[int] = None
#     extra_data_dict: dict = {}


class RDCartCreate(BaseRDCartRequired):
    pass


class RDCart(BaseRDCartRequired):
    pass


# class RDCartBulked(BaseModel):
#     total: int 
#     items: List[BaseRDCart]


class RDCartWithCount(BaseModel):
    total: Decimal
    products: List[RDCart]


class RDCartItemsCount(BaseModel):
    items_count: int


class RDCartLessQuantity(BaseModel):
    new_stock: int
    deleted: bool


class RDCartNewQuantity(BaseModel):
    new_quantity: int


class RDCartSubtotal(BaseModel):
    subtotal: Union[condecimal(decimal_places=2), str]