from app.db.db_setup import Base
from app.models import user

import sqlalchemy as sa 
 

class OrderItem(Base):
    __tablename__ = 'order_item'

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id', ondelete="CASCADE"))
    item_id = sa.Column(sa.Integer, sa.ForeignKey('item.id', ondelete="CASCADE"))
    quantity = sa.Column(sa.Integer, default=1)

    def __repr__(self) -> str:
        return "<OrderItem(id=%s, item_id=%s)>" % (self.id, self.item_id)

order_item = OrderItem.__table__