from app.db.db_setup import Base
from app.models import order_item

from sqlalchemy.orm import relationship, backref 
import sqlalchemy as sa 

class Item(Base):
    __tablename__ = 'item'

    id = sa.Column(sa.Integer, primary_key=True)
    category_id = sa.Column(sa.Integer, sa.ForeignKey('category.id', ondelete="CASCADE"), nullable=False)
    name = sa.Column(sa.String(100), unique=True)
    description = sa.Column(sa.Text)
    stock = sa.Column(sa.Integer, default=1)
    sold = sa.Column(sa.Integer, default=0, nullable=True)
    timestamp = sa.Column(sa.DateTime, server_default=sa.sql.func.now())
    availability = sa.Column(sa.Boolean, default=True)
    is_active = sa.Column(sa.Boolean, default=True)
    featured = sa.Column(sa.Boolean, default=False)
    image_url = sa.Column(sa.String(100))
    unit_price = sa.Column(sa.Numeric(10, 2))
    discount_price = sa.Column(sa.Numeric(10, 2), nullable=True)


    
    relationship('OrderItem', cascade="all,delete", backref=backref('item', cascade="all,delete"))


    def __repr__(self):
        return "<Item(id=%s, name=%s)>" % (self.id, self.name)


item = Item.__table__