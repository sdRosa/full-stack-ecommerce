from app.models import order_item, order, address
from app.db.db_setup import Base

from sqlalchemy_utils.types.email import EmailType
from sqlalchemy.orm import relationship, backref 

import sqlalchemy as sa


class User(Base):
    __tablename__ = 'user'

    id = sa.Column(sa.BigInteger, primary_key=True)
    address_id = sa.Column(sa.Integer, sa.ForeignKey('address.id', ondelete="CASCADE"), nullable=True)
    default_address = sa.Column(sa.Boolean, default=False)
    name = sa.Column(sa.String(50))
    lastname = sa.Column(sa.String(50))
    email = sa.Column(EmailType)
    password = sa.Column(sa.String(75))
    is_active = sa.Column(sa.Boolean, default=True)
    scopes = sa.Column(sa.ARRAY(sa.String()))

    relationship('OrderItem', cascade="all,delete", backref=backref('user', cascade="all,delete"))
    relationship('Order', cascade="all,delete", backref=backref('user', cascade="all,delete"))

    def __repr__(self):
        return "<User(id=%s, email=%s)>" % (self.id, self.email)


user = User.__table__

