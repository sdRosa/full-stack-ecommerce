from app.db.db_setup import Base
from app.models import user, item, order

from sqlalchemy.orm import relationship, backref
import sqlalchemy as sa 


class Address(Base):
    __tablename__ = 'address'

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id', ondelete='CASCADE'))
    street_address = sa.Column(sa.String(100))
    apartment_address = sa.Column(sa.String(100))
    province = sa.Column(sa.String(2))
    zip = sa.Column(sa.String(4))
    payment_note = sa.Column(sa.String(200), nullable=True)

    relationship('Order', backref=backref('order', passive_deletes=True))
    relationship('User', backref=backref('order', passive_deletes=True))


    def __repr__(self) -> str:
        return '<Address(id=%s, user_id=%s)>' % (self.id, self.user_id)


address = Address.__table__