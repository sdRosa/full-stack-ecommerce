from app.db.db_setup import Base
from app.models import item
from sqlalchemy.orm import relationship, backref 

import sqlalchemy as sa 
import datetime

class Category(Base):
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(50))
    created_at = sa.Column(sa.DateTime, server_default=sa.sql.func.now())
    is_active = sa.Column(sa.Boolean, default=True)

    relationship('Item', cascade="all,delete", backref=backref('category', cascade="all,delete"))

category = Category.__table__
