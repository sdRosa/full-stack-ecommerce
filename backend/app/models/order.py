from app.db.db_setup import Base
from app.models import order_item, order

from sqlalchemy.orm import relationship, backref
import sqlalchemy as sa 


class OIAssociation(Base):
    __tablename__ = 'order_items_association'

    id = sa.Column(sa.Integer, primary_key=True)
    order_id = sa.Column(sa.Integer, sa.ForeignKey('order.id'))
    order_items_id = sa.Column(sa.Integer, sa.ForeignKey('order_item.id'))
  
 
class Order(Base):
    __tablename__ = 'order'

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    address_id = sa.Column(sa.Integer, sa.ForeignKey('address.id', ondelete='SET NULL'), nullable=True)
    created_at = sa.Column(sa.DateTime, server_default=sa.sql.func.now())
    payment = sa.Column(sa.SmallInteger)
    subtotal = sa.Column(sa.Numeric(10, 2))
    items = relationship(
        'order_items', 
        secondary=OIAssociation, 
        backref=backref('order', lazy='dynamic'))

    def __repr__(self) -> str:
        return "<Order(id=%s, user_id=%s)>" % (self.id, self.user_id)

order = Order.__table__
order_bound = OIAssociation.__table__