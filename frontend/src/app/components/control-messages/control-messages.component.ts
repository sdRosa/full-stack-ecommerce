import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { ValidationService } from '../../services/validations/validation.service';

@Component({
  selector: 'control-messages',
  template: `
      <span *ngIf="errorMessage !== null" style="color:red; font: 5px; margin-top: 2px;">{{ errorMessage }}</span>
  `
})
export class ControlMessagesComponent implements OnInit {
  @Input() control!: FormControl;

  constructor() { }

  ngOnInit(): void {
  }

  get errorMessage() {
    for (let propertyName in this.control.errors) {
      if (
        this.control.errors.hasOwnProperty(propertyName) &&
        this.control.touched
      ) {
        return ValidationService.getValidatorErrorMessage(
          propertyName,
          this.control.errors[propertyName]
        ) 
      }
    }
  }
}
