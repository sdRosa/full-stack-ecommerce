import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { AddressModel } from 'src/app/interfaces/address.interface';
import { BulkCartsModel } from 'src/app/interfaces/order.interface';

import { OrderService } from 'src/app/services/order.service';
import { CartModelServer } from 'src/app/interfaces/cart.interface';
import { AddressService } from 'src/app/services/address.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { UserService } from 'src/app/services/user.service';
import { CartService } from 'src/app/services/cart.service';
import { ToastrService } from 'ngx-toastr';

import { maxLength, RxwebValidators } from '@rxweb/reactive-form-validators';


export enum ProvincesEnum {
  BA = 'Buenos Aires',
  CO = 'Cordoba',
  MZ = 'Mendoza',
  MI = 'Misiones',
}

enum PaymentTypes {
  'Tranferience'= 0,
  'Cash' = 1,
  'Paypal' = 2
}


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  cartData: CartModelServer = {
    total: 0,
    products: []
  };
  cartTotal: number = 0;

  provincesType = ProvincesEnum;
  paymentType = PaymentTypes;
  addressForm: FormGroup;
  paymentForm: FormGroup;

  private reDefaultValidator = RxwebValidators.required({
    conditionalExpression:(x, y) => y.use_default_address == false}
  )

  constructor(private fb: FormBuilder,
              private orderService: OrderService,
              private addressService: AddressService,
              private userService: UserService,
              public cartService: CartService,
              private tokenService: TokenStorageService,
              private router: Router,
              private toast: ToastrService
    ) { 
      this.addressForm = this.fb.group({
        street_address: ['', [this.reDefaultValidator, Validators.maxLength(100)]],
        aparment_address: ['', [this.reDefaultValidator, Validators.maxLength(100)]],
        province: ['', [this.reDefaultValidator, Validators.maxLength(2)]],
        zip: ['', [this.reDefaultValidator, Validators.maxLength(4), Validators.minLength(4)]],
        set_default_address: [false, []],
        use_default_address: [false, []],
        payment_note: ['', [Validators.maxLength(200)]],
      });
      this.paymentForm = this.fb.group({
        payment: ['', [Validators.required]] 
      })  
  }
 
  get isNotValid() {
    return !this.addressForm.valid || !this.paymentForm.valid
  }

  changeProvince(e) {
    this.addressForm.setValue(e.target.value, {
      onlySelf: true,
    })
  }
  
  createOrder() {
    if (
        (this.addressForm.dirty && this.addressForm.valid) && 
        (this.paymentForm.dirty && this.paymentForm.valid)
      ) {
        let user_id = JSON.parse(this.tokenService.getUserId());
        if (this.addressForm.value.use_default_address) {
          this.userService.getSelfUser().subscribe(
            userData => {
              if (userData.default_address && userData.address_id) {
                let newOrder: BulkCartsModel = {
                  payment: this.paymentForm.value.payment,
                  address_id: userData.address_id,
                  user_id: user_id
                }
                this.orderService.createOrder(newOrder).subscribe(
                  (orderId: {order_id: number}) => {
                    const navigationExtras: NavigationExtras = {
                      state: {
                        'orderId': orderId.order_id,
                        'orderData': this.cartService.cartData,
                      }
                    } 
                    return this.router.navigate(['thankyou'], navigationExtras).then(r => {
                      this.cartService.resetCartData();
                      this.cartService.cartTotal$.next(0);
                    })
                  },
                  (err: HttpErrorResponse) => {
                    switch (err.status) {
                      case 404:
                        break;
                      case 405:
                        break;
                      case 502:
                        break;
                    }
                  }
                );
              } else {
                this.toast.warning("User doesn't have a default address", 'No Default Address', {
                  timeOut: 3000,
                  progressBar: true,
                  progressAnimation: 'increasing',
                  positionClass: 'toast-bottom-left'
                });
              }
            }
          );
        } else {
          const valAddress: AddressModel = {
            user_id: user_id,
            street_address: this.addressForm.value.street_address,
            apartment_address: this.addressForm.value.aparment_address,
            province: this.addressForm.value.province,
            zip: this.addressForm.value.zip,
            payment_note: this.addressForm.value.payment_note,
          };
  
          this.addressService.addAddress(
            this.addressForm.value.set_default_address, 
            valAddress).subscribe(
            data => {
              let newOrder: BulkCartsModel = {
                payment: this.paymentForm.value.payment,
                address_id: data.id,
                user_id: user_id
              };
              this.orderService.createOrder(newOrder).subscribe(
                (orderId: {order_id: number}) => {
                  const navigationExtras: NavigationExtras = {
                    state: {
                      'orderId': orderId.order_id,
                      'orderData': this.cartService.cartData,
                    }
                  }
                  return this.router.navigate(['thankyou'], navigationExtras).then(r => {
                        this.cartService.resetCartData();
                        this.cartService.cartTotal$.next(0);
                      });
                },
                (err: HttpErrorResponse) => {
                  switch (err.status) {
                    case 404:
                      break;
                    case 405:
                      break;
                    case 502:
                      break;
                  }
                }
              );
            }
          );
        }
    }
  }

  ngOnInit(): void {
    this.cartService.cartTotal$.subscribe(total => this.cartTotal = total);
    this.cartService.cartData$.subscribe(data => this.cartData = data);
  }

}

