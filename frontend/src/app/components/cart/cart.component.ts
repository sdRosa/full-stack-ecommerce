import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { CartService } from 'src/app/services/cart.service';
import { AuthService } from 'src/app/services/auth.service';

import { CartItem, CartModelServer } from '../../interfaces/cart.interface';
import { ItemService } from 'src/app/services/item.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartData: CartModelServer = {
    total: 0,
    products: []
  };
  cartTotal: number = 0;
  logged!: boolean

  staticDomain: string = environment.BACKEND_DOMAIN;

  constructor(
    public cartService: CartService,
    private authService: AuthService,
    private router: Router
  ) { }

  selectItem(id: number) {
    return this.router.navigate(['/item', id]).then();
  }

  ngOnInit(): void {
    this.authService.checkAuth()
    this.authService.logged$.subscribe(logged => this.logged = logged);
    if (this.logged) {
      this.cartService.cartTotal$.subscribe(total => this.cartTotal = total);
      this.cartService.cartData$.subscribe(data => this.cartData = data);
    }
  }
}
