import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationExtras } from '@angular/router';

import { environment } from 'src/environments/environment';
import { CatNames } from 'src/app/interfaces/categories.interface';
import { CartModelServer } from '../../interfaces/cart.interface';
import { CartService } from 'src/app/services/cart.service';
import { AuthService } from 'src/app/services/auth.service';
import { CategoriesService } from 'src/app/services/categories.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
 
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  logged!: boolean
  userName!: string
  catNames!: CatNames[]
  cartData: CartModelServer = {
    total: 0,
    products: []
  };
  cartTotal: number = 0;

  staticDomain: string = environment.BACKEND_DOMAIN; 

  constructor(
    private tokenService: TokenStorageService,
    public authService: AuthService,
    public cartService: CartService,
    private catService: CategoriesService,
    private router: Router,
    public loc: Location
  ) { }

  ngOnInit(): void {
    this.authService.checkAuth();
    this.authService.logged$.subscribe(logged => this.logged = logged); 
    this.catService.getCatNames().subscribe(
      catsNames => {
        this.catNames = catsNames;
      }
    );
    if (this.logged) {
      this.cartService.cartTotal$.subscribe(total => this.cartTotal = total);
      this.cartService.cartData$.subscribe(data => this.cartData = data);
      this.userName = this.tokenService.getName();
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/']).then(() => this.logged = false)
  }

  selectItem(id: number) {
    return this.router.navigate(['/item', id]).then();
  }

  selectCat(catId: number, catName: string) {
    const navigationExtras: NavigationExtras = {
      state: {
        'catName': catName,
      }
    }
    return this.router.navigate(['shop/categories', catId], navigationExtras);

  }

}
