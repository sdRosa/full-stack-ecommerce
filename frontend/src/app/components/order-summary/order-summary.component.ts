import { Component, OnInit } from '@angular/core';
import { CartModelServer } from 'src/app/interfaces/cart.interface';
import { CartService } from 'src/app/services/cart.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit {
  staticDomain = environment.BACKEND_DOMAIN

  cartData: CartModelServer = {
    total: 0,
    products: []
  };
  cartTotal: number = 0;

  constructor(public cartService: CartService) { }

  ngOnInit(): void {
    this.cartService.cartTotal$.subscribe(total => this.cartTotal = total);
    this.cartService.cartData$.subscribe(data => this.cartData = data);
  }

  calculateSubtotal(index: number) {
    this.cartService.calculateSubtotal(index);
  }

  changeQuantity(index: number, increase: boolean) {
    this.cartService.updateCartItems(index, increase);
  }

  deleteItemCart(index: number) {
    this.cartService.deleteItemCart(index);
  }

}
