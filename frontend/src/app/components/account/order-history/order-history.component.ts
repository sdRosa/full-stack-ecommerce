import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { OrderServer } from 'src/app/interfaces/order.interface';
import { OrderService } from 'src/app/services/order.service';
import { environment } from 'src/environments/environment';



@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {
  staticDomain = environment.BACKEND_DOMAIN

  ordersList!: OrderServer[]

  page = 1
  count = 0
  pageSize = 5


  constructor(private orderService: OrderService) { }

  ngOnInit(): void {
    this.getItems();
  }

  getItems() {
    const actualIndex = this.page - 1;
    this.orderService.getOwnOrders(actualIndex, this.pageSize).subscribe(
      data => {
        const { orders, total_orders } = data;
        this.ordersList = orders;
        this.count = total_orders;
      },
      (err: HttpErrorResponse) => {
        switch (err.status) {
          case 404:
            break;
        }
      }
    );
  }

  handlePageChange(event) {
    this.page = event;
    this.getItems();
  }

  handlePageSizeChange(event) {
    this.pageSize = event.target.value;
    this.getItems();
  }


}
