import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { AddressService } from 'src/app/services/address.service';
import { User } from 'src/app/interfaces/user.interface';
import { OrderService } from 'src/app/services/order.service';
import { UserService } from 'src/app/services/user.service';
import { AddressModelServer } from 'src/app/interfaces/address.interface';
import { OrderServer } from 'src/app/interfaces/order.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  selfUser!: User
  userAddress!: AddressModelServer
  latestOrders!: OrderServer[]

  constructor(
    private userService: UserService,
    private addressService: AddressService,
    private orderService: OrderService,
  ) { }

  ngOnInit(): void {
    this.userService.getSelfUser().subscribe(
      userData => {
        this.selfUser = userData;
        if (userData.default_address) {
          this.addressService.getAddress(userData.address_id).subscribe(
            defaultAddress => {
              this.userAddress = defaultAddress; 
            },
            (err: HttpErrorResponse) => {
              switch (err.status) {
                case 404:
                  break;
              }
            }
          );
        } else {
          this.addressService.getLatestAddress().subscribe(
            latestAddress => {
              this.userAddress = latestAddress;
            },
            (err: HttpErrorResponse) => {
              switch (err.status) {
                case 404:
                  break;
              }
            }
          )
        }
      }
    )
    this.orderService.getOrdersSorted(0, 3, 'created_at').subscribe(
      orders => {
        this.latestOrders = orders;
      }
    )
  }

}
