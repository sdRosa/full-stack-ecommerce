import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddresessComponent } from './addresess.component';

describe('AddresessComponent', () => {
  let component: AddresessComponent;
  let fixture: ComponentFixture<AddresessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddresessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddresessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
