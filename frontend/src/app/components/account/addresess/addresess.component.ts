import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AddressModelServer } from 'src/app/interfaces/address.interface';
import { User } from 'src/app/interfaces/user.interface';
import { AddressService } from 'src/app/services/address.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-addresess',
  templateUrl: './addresess.component.html',
  styleUrls: ['./addresess.component.css']
})
export class AddresessComponent implements OnInit {
  defaultAddress!: AddressModelServer | undefined
  defaultAddressIndex!: number
  selfAddresses!: AddressModelServer[]
  selfUser!: User
  

  constructor(private addressService: AddressService,
              private userService: UserService,
              private toast: ToastrService,
              private router: Router) { } 

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.addressService.getOwnAddresses(0, 100).subscribe(
      ownAddresses => {
        this.selfAddresses = ownAddresses;
        this.userService.getSelfUser().subscribe(
          selfUser => {
            this.selfUser = selfUser;
            if (this.selfUser.default_address && this.selfUser.address_id) {
              let index = this.selfAddresses.findIndex(a => a.id == this.selfUser.address_id);
              if (index != -1) {
                this.defaultAddress = this.selfAddresses.splice(index, 1)[0];
                this.defaultAddressIndex = index;
              }
            }
          }
        )
      }
    )
  }

  updateDefault(set_default: boolean, addressId: number) {
    this.userService.updateDefaultAddress(set_default, addressId).subscribe(
      updatedAddress => {
        let index = this.selfAddresses.findIndex(a => a.id == updatedAddress.address_id);
        if (updatedAddress.default_address) {
          if (index != -1) {
            this.defaultAddress = this.selfAddresses.splice(index, 1)[0];
            this.getData();
            this.toast.success(`Address set up as default`, 'Default Address', {
              timeOut: 1500,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-bottom-left'
            });
          }
        } else {
          this.selfAddresses.push(this.defaultAddress!)
          this.defaultAddress = undefined
          this.toast.success(`Default address removed`, 'Default Address', {
            timeOut: 1500,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-bottom-left'
          });
        }
      },
      
    )
  }


  updateAddress(addressId: number, index: number) {
    if (this.defaultAddress?.id == addressId) {
      const navExtras: NavigationExtras = {
        state: {
          'addressToUpdate': this.defaultAddress
        }
      }
      this.router.navigate(['account/addresses', addressId], navExtras)

    } else {
      const navExtras: NavigationExtras = {
        state: {
          'addressToUpdate': this.selfAddresses[index]
        }
      }
      this.router.navigate(['account/addresses', addressId], navExtras)
    }
  }


  delAddress(addressId: number, index: number) {
    this.addressService.delOwnAddress(addressId).subscribe(
      msg => {
        if (this.defaultAddress?.id == addressId) {
          this.defaultAddress = undefined
        } else {
          this.selfAddresses.splice(index, 1)
        }
        this.toast.success(`Address deleted successfully`, 'Address Deleted', {
          timeOut: 1500,
          progressBar: true,
          progressAnimation: 'increasing',
          positionClass: 'toast-bottom-left'
        });
      },
      (err: HttpErrorResponse) => {
        switch(err.status) {
          case 404: 
            this.toast.warning(err.error.msg, 'Address Not Found', {
              timeOut: 3000,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-bottom-left'
            });
            break
          case 502: 
          this.toast.warning(err.error.msg, 'Order Dependance', {
            timeOut: 3000,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-bottom-left'
          });
          break
          case 401: 
          this.toast.warning(err.error.msg, 'Wrong Address User', {
            timeOut: 3000,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-bottom-left'
          });
          break
        }
      }
    )
  }


}

