import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddresessEditComponent } from './addresess-edit.component';

describe('AddresessEditComponent', () => {
  let component: AddresessEditComponent;
  let fixture: ComponentFixture<AddresessEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddresessEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddresessEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
