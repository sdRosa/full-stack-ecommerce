import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { AddressModel } from 'src/app/interfaces/address.interface';
import { AddressService } from 'src/app/services/address.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';


export enum ProvincesEnum {
  BA = 'Buenos Aires',
  CO = 'Cordoba',
  MZ = 'Mendoza',
  MI = 'Misiones',
}

@Component({
  selector: 'app-addresess-edit',
  templateUrl: './addresess-edit.component.html',
  styleUrls: ['./addresess-edit.component.css']
})
export class AddresessEditComponent implements OnInit {
  dbAddress!: AddressModel
  provincesType = ProvincesEnum
  addressForm: FormGroup

  constructor(
    private addressService: AddressService,
    private tokenService: TokenStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private toast: ToastrService
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras.state as {
      addressToUpdate: AddressModel
    };
    this.dbAddress = state.addressToUpdate;
    this.addressForm = this.fb.group({
      street_address: [this.dbAddress.street_address, [Validators.required, Validators.maxLength(100)]],
      apartment_address: [this.dbAddress.apartment_address, [Validators.required, Validators.maxLength(100)]],
      province: [this.dbAddress.province, [Validators.required, Validators.maxLength(2)]],
      zip: [this.dbAddress.zip, [Validators.required, Validators.maxLength(4), Validators.minLength(4)]],
      payment_note: [this.dbAddress.payment_note, [Validators.maxLength(200)]],
    });
  }

  ngOnInit(): void { }

  get isNotValid() {
    return !this.addressForm.valid
  }

  updateAddress() {
    if (this.addressForm.dirty && this.addressForm.valid) {
      const userId = JSON.parse(this.tokenService.getUserId())

      const toUpdateAddress: AddressModel = {
        user_id: userId,
        street_address: this.addressForm.value.street_address,
        apartment_address: this.addressForm.value.apartment_address,
        province: this.addressForm.value.province,
        zip: this.addressForm.value.zip,
        payment_note: this.addressForm.value.payment_note,

      };
      this.route.paramMap.pipe(
        map(param => {
          // @ts-ignore
          return param.params.addressId
        })
      ).subscribe(addressId=> {
        this.addressService.updateAddress(addressId, toUpdateAddress).subscribe(
          updatedAddress => {
            this.router.navigate(['account/addresses']).then(r => {
              this.toast.success(`Address updated successfully`, 'Address Updated', {
                timeOut: 3000,
                progressBar: true,
                progressAnimation: 'increasing',
                positionClass: 'toast-bottom-left'
              });
            })
          },
          (err: HttpErrorResponse) => {
            switch (err.status) {
              case 404:
                this.router.navigate(['account/addresses']).then(r => {
                  this.toast.warning(`This address doesn't exists`, 'Address Not Found', {
                    timeOut: 3000,
                    progressBar: true,
                    progressAnimation: 'increasing',
                    positionClass: 'toast-bottom-left'
                  });
                });
                break;
              case 401:
                this.router.navigate(['account/addresses']).then(r => {
                  this.toast.warning(`You are not the owner of this address`, 'Not Authorized', {
                    timeOut: 3000,
                    progressBar: true,
                    progressAnimation: 'increasing',
                    positionClass: 'toast-bottom-left'
                  });
                });
                break;
              }
            }
          );
        });
      }
    }

}
