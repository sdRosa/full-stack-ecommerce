import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { ProvincesEnum } from 'src/app/components/checkout/checkout.component'
import { AddressModel } from 'src/app/interfaces/address.interface';
import { AddressService } from 'src/app/services/address.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';


@Component({
  selector: 'app-addresess-add',
  templateUrl: './addresess-add.component.html',
  styleUrls: ['./addresess-add.component.css']
})
export class AddresessAddComponent implements OnInit {
  addressForm: FormGroup
  provincesType = ProvincesEnum

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private toast: ToastrService,
    private addressService: AddressService,
    private tokenService: TokenStorageService,
  ) {
    this.addressForm = this.fb.group({
      street_address: ['', [Validators.required, Validators.maxLength(100)]],
      aparment_address: ['', [Validators.required, Validators.maxLength(100)]],
      province: ['', [Validators.required, Validators.maxLength(2)]],
      zip: ['', [Validators.required, Validators.maxLength(4), Validators.minLength(4)]],
      set_default_address: [false, []],
      payment_note: ['', [Validators.maxLength(200)]],
    });
  }

  addAddress() {
    if (this.addressForm.dirty && this.addressForm.valid) {
      const userId = JSON.parse(this.tokenService.getUserId());

      const valAddress: AddressModel = {
        user_id: userId,
        street_address: this.addressForm.value.street_address,
        apartment_address: this.addressForm.value.aparment_address,
        province: this.addressForm.value.province,
        zip: this.addressForm.value.zip,
        payment_note: this.addressForm.value.payment_note,
      }
      this.addressService.addAddress(
        this.addressForm.value.set_default_address, 
        valAddress
      ).subscribe(
        data => {
          this.router.navigate(['account/addresses']).then(r => {
            this.toast.success(`Address added successfully`, 'Address Added', {
              timeOut: 1500,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-bottom-left'})
          });
        }
      )
    }
  }
   

  ngOnInit(): void { } 

}
