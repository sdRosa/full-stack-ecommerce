import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddresessAddComponent } from './addresess-add.component';

describe('AddresessAddComponent', () => {
  let component: AddresessAddComponent;
  let fixture: ComponentFixture<AddresessAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddresessAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddresessAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
