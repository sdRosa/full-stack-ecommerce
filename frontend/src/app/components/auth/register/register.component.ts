import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { shareReplay } from 'rxjs/operators';
import { userResult } from 'src/app/interfaces/auth.interface';
import { AuthService } from 'src/app/services/auth.service';
import { ValidationService } from '../../../services/validations/validation.service';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router 
    ) {
      this.userForm = this.fb.group({
        name: ['', [Validators.required]],
        lastname: [''],
        email: ['', [Validators.required, ValidationService.emailValidator]],
        password: ['', [Validators.required, ValidationService.passwordValidator]],
        password2: ['', [Validators.required, ValidationService.passwordValidator]]
      });          
    }

    public register() {
      if (this.userForm.dirty && this.userForm.valid) {
        const valName: string = this.userForm.value.name
        const valLastname: string = this.userForm.value.lastname
        const valEmail: string = this.userForm.value.email
        const valPass: string = this.userForm.value.password
        const valPass2: string = this.userForm.value.password2

        this.authService.register(
          {
            name: valName,
            lastname: valLastname,
            email: valEmail, 
            password: valPass, 
            password2: valPass2
          }
        ).pipe(shareReplay(1)).subscribe(
          (data: userResult) => {
            this.router.navigateByUrl('login');
          },
          err => {
          }
        )
      }
    }

    ngOnInit(): void { }

}
