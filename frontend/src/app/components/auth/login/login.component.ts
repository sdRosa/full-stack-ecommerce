import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { shareReplay } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { ValidationService } from '../../../services/validations/validation.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userForm: FormGroup;
  data$: any;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private toast: ToastrService,
    private router: Router 
    ) {
      this.userForm = this.fb.group({
        email: ['', [Validators.required, ValidationService.emailValidator]],
        password: ['', [Validators.required, ValidationService.passwordValidator]]
      });          
    }

    login() {
      
      if (this.userForm.dirty && this.userForm.valid) {
        const valEmail: string = this.userForm.value.email
        const valPass: string = this.userForm.value.password
        this.authService.login(valEmail, valPass).pipe(shareReplay(1)).subscribe(
          data => {
            this.tokenStorage.saveToken(data.access_token);
            this.tokenStorage.saveName(data.username);
            this.tokenStorage.saveUserId(data.user_id);
            this.tokenStorage.saveTTL(data.ttl);
            this.authService.logged$.next(true);
            this.router.navigateByUrl('/');
          },  
          (err: HttpErrorResponse) => {
            switch (err.status) {
              case 400:
                this.toast.info(`Inactive User or incorrect email or password`, 'Invalid Credentials', {
                  timeOut: 3000,
                  progressBar: true,
                  progressAnimation: 'increasing',
                  positionClass: 'toast-bottom-right'
                });
                break
            }
          }
        );
      }
    }

  ngOnInit(): void { }
}
