import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

import { ItemService } from '../../services/item.service';
import { CartService } from '../../services/cart.service';
import { CategoriesService } from '../../services/categories.service';
import { ProductPub } from '../../interfaces/product.interface';
import { CatNames } from '../../interfaces/categories.interface';
import { environment } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  staticDomain = environment.BACKEND_DOMAIN
  prodList!: ProductPub[]
  latestProds!: ProductPub[]
  catNames!: CatNames[]

  page = 1
  count = 0
  pageSize = 3
  pageSizes = [3, 6, 9]
  sortBy = ['datetime', 'sold']

  constructor(private itemService: ItemService,
              private catService: CategoriesService,
              private cartService: CartService,
              private router: Router) { }

  ngOnInit(): void {
    this.getItems();
    this.itemService.getAllSorted(0, 5, 'datetime').subscribe(
      data => {
        this.latestProds = data;
      }
    )
    this.catService.getCatNames().subscribe(
      catsNames => {
        this.catNames = catsNames;
      }
    )
  }

  getItems(orderBy: string = '') {
    const actualIndex = this.page - 1;
    this.itemService.getItemsShop(actualIndex, this.pageSize, orderBy).subscribe(
      data => {
        const { items, total_items } = data;
        this.prodList = items;
        this.count = total_items;
      },
      (err: HttpErrorResponse) => {
        switch (err.status) {
          case 404:
            break;
        }
      }
    );
  }

  handlePageChange(event) {
    this.page = event;
    this.getItems();
  }

  handlePageSizeChange(event) {
    this.pageSize = event.target.value;
    this.getItems();
  }

  handleSortByChange(event) {
    this.getItems(event.target.value);
  }

  addToCart(itemId: number) {
    this.cartService.addItemToCart(itemId);
  }

  selectItem(id: number) {
    return this.router.navigate(['/item', id]).then();
  }

  selectCategory(id: number, catName: string) {
    const navigationExtras: NavigationExtras = {
      state: {
        'catName': catName,
      }
    }
    this.router.navigate(['shop/categories', id], navigationExtras).then();
  }
}
