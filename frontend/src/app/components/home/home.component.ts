import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ItemService } from '../../services/item.service';
import { CartService } from '../../services/cart.service';
import { ProductPub } from '../../interfaces/product.interface';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  rawProducts: ProductPub[] = [];

  featuredProducts: ProductPub[] = [];
  newProducts: ProductPub[] = [];
  discountPriceProducts: ProductPub[] = [];
  bestSellerProducts: ProductPub[] = [];
  bestSellerProductsMin: ProductPub[] = [];

  selectedSoldProduct!: ProductPub;

  staticDomain: string = environment.BACKEND_DOMAIN;

  constructor(private itemService: ItemService,
              private cartService: CartService,
              private router: Router) { }

  ngOnInit(): void {
    this.itemService.getAllItems(0, 10).subscribe((items: ProductPub[]) => {
      this.featuredProducts = items.filter(prod => prod.featured == true).slice(0, 3);
      this.newProducts = items.sort((prod1, prod2) => prod1.timestamp - prod2.timestamp);
      this.discountPriceProducts = items.filter(prod => prod.discount_price != null);

      let selectedSoldProduct = items.sort((prod1, prod2) =>  prod2.sold - prod1.sold );
      this.bestSellerProductsMin = selectedSoldProduct.slice(0, 3);
      let soldProductSorted = selectedSoldProduct.shift();
      this.bestSellerProducts = selectedSoldProduct;
      this.selectedSoldProduct = soldProductSorted!;
    })
  }

  addToCart(itemId: number) {
    this.cartService.addItemToCart(itemId);
  }

  selectItem(id: number) {
    return this.router.navigate(['/item', id]);
  }

}
