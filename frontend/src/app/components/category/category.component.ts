import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras, NavigationEnd } from '@angular/router';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { CategoriesService } from 'src/app/services/categories.service';
import { ItemService } from '../../services/item.service';
import { CartService } from '../../services/cart.service';
import { ProductPub } from '../../interfaces/product.interface';
import { CatNames } from 'src/app/interfaces/categories.interface';
import { environment } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  staticDomain = environment.BACKEND_DOMAIN
  navigationSubscription;
  prodList!: ProductPub[]
  latestProds!: ProductPub[]
  catNames!: CatNames[] 

  id!: number
  catName!: string
  page = 1
  count = 0
  pageSize = 3
  pageSizes = [3, 6, 9]
  sortBy = ['datetime', 'sold']

  constructor(
    private cartService: CartService,
    private catService: CategoriesService,
    private itemService: ItemService,
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastrService
    ) { 
      this.navigationSubscription = this.router.events.subscribe((e: any) => {
        if (e instanceof NavigationEnd) {
          this.initialiseInvites();
        }
      });
  }

  initialiseInvites() {
    this.route.paramMap.pipe(
      map(param => {
        // @ts-ignore
        return param.params.cat_id
      })
    ).subscribe(
      catId => {
        this.id = catId;
        const navigation = this.router.getCurrentNavigation();
        const state = navigation?.extras.state as {
          catName: string,
        };
        if (state != undefined) {
          this.catName = state.catName;
        } else {
          this.catService.getCatName(this.id).subscribe(
            catName => {
              this.catName = catName;
            },
            (err: HttpErrorResponse) => {
              switch (err.status) {
                case 404:
                  this.router.navigate(['shop']).then(r => {
                    this.toast.warning(`This category doesn't exists`, 'Category Not Found', {
                      timeOut: 1500,
                      progressBar: true,
                      progressAnimation: 'increasing',
                      positionClass: 'toast-bottom-left'
                    });
                  }
                );
              } 
            }
          );
        }
      }
    )
    this.getItems();
    this.itemService.getAllSorted(0, 5, 'datetime').subscribe(
      data => {
        this.latestProds = data;
      }
    )
    this.catService.getCatNames().subscribe(
      catsNames => {
        this.catNames = catsNames;
      }
    )
  }


  ngOnInit(): void {
    
  }


  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe()
    }
  }


  getItems(orderBy: string = '') {
    const actualIndex = this.page - 1;
    
    this.itemService.getItemsShopByCat(this.id, actualIndex, this.pageSize, orderBy).subscribe(
      data => {
        const { items, total_items } = data;
        this.prodList = items;
        this.count = total_items;
      },
      (err: HttpErrorResponse) => {
        switch (err.status) {
          case 404:
            break;
        }
      }
    );
  }

  handlePageChange(event) {
    this.page = event;
    this.getItems();
  }

  handlePageSizeChange(event) {
    this.pageSize = event.target.value;
    this.getItems();
  }

  handleSortByChange(event) {
    this.getItems(event.target.value);
  }

  addToCart(itemId: number) {
    this.cartService.addItemToCart(itemId);
  }

  selectItem(id: number) {
    return this.router.navigate(['/item', id]).then();
  }

  selectCategory(event, catId: number) {
    this.catName = event.target.innerText;
    this.id = catId;
    const navigationExtras: NavigationExtras = {
      state: {
        'catName': this.catName,
      }
    }
    this.router.navigate(['shop/categories', this.id], navigationExtras).then(r => {
      this.getItems();
    });
  }

}
