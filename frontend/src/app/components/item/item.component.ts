import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ProductPub, ProductByCatPriv } from 'src/app/interfaces/product.interface';
import { CartService } from 'src/app/services/cart.service';
import { ItemService } from 'src/app/services/item.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  staticDomain = environment.BACKEND_DOMAIN

  id!: number
  prod!: ProductPub
  relatedProds!: ProductByCatPriv

  constructor(private router: Router,
              private route: ActivatedRoute,
              private itemService: ItemService,
              private cartService: CartService) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      map(param => {
        // @ts-ignore
        return param.params.item_id
      })
    ).subscribe(prodId => {
      this.id = prodId;
      this.itemService.getItem(prodId).subscribe(
        prod => {
          this.prod = prod;
          this.itemService.getAllItemsByCat(this.prod.category_id!, 0, 5).subscribe(
            prods => {
              let filteredProds = {
                cat_name: prods.cat_name,
                items: prods.items.filter(i => i.id != this.prod.id)
              }
              this.relatedProds = filteredProds;
            }
          )
        },
        (err: HttpErrorResponse) => {
          switch (err.status) {
            case 404:
              return this.router.navigate['404']
          }
        }
      );
      
    });
  }

  addToCart(prodId: number) {
    this.cartService.addItemToCart(prodId);
  }

  selectItem(id: number) {
    return this.router.navigate(['/item', id]).then();
  }
}
