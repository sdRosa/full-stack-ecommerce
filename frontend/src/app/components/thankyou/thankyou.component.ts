import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartModelServer } from 'src/app/interfaces/cart.interface';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class ThankyouComponent implements OnInit {
  staticDomain = environment.BACKEND_DOMAIN
  orderId: number = 0
  orderData: CartModelServer = {
    total: 0,
    products: []
  }

  constructor(private router: Router) { 
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras.state as {
      orderId: number,
      orderData: CartModelServer
    };
    this.orderId = state.orderId;
    this.orderData = state.orderData;
  }

  ngOnInit(): void {
  }

}
