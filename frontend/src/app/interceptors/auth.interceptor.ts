import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { TokenStorageService } from 'src/app/services/token-storage.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constuctor() {}
    intercept(
        req: HttpRequest<any>,
        next: HttpHandler,
    ): Observable<HttpEvent<any>> {
        const Token = localStorage.getItem('auth-token');

        if (Token && this.isValidRequestForInterceptor(req.url)) {
            const cloned = req.clone({
                headers: req.headers.set("Authorization",
                                         "Bearer " + Token)
            });
            return next.handle(cloned)
        } else {
            return next.handle(req)
        }
    }
    private isValidRequestForInterceptor(requestUrl: string): boolean {
        const urlsToNotUse = [
            'login/access-token',
            'users/register',
            'items/all'
        ];
        let positionIndicator: string = 'api/';
        let position = requestUrl.indexOf(positionIndicator);
        if (position > 0) {
            let destination: string = requestUrl.substr(position + positionIndicator.length);
            for (let address of urlsToNotUse) {
                if (new RegExp(address).test(destination)) {
                    return false;
                }
            }
        }
        return true
    }
}

export const AuthInterceptorProviders = [
    { 
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
    }
];