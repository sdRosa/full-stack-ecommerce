import { NgModule } from '@angular/core';
import { BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { JwtModule } from '@auth0/angular-jwt';
 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ItemComponent } from './components/item/item.component';
import { CartComponent } from './components/cart/cart.component';
import { ControlMessagesComponent } from './components/control-messages/control-messages.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';

import { AuthInterceptorProviders } from './interceptors/auth.interceptor';
import { SafePipe } from './utils/sanitizer.pipe';
import { OrderHistoryComponent } from './components/account/order-history/order-history.component';
import { DashboardComponent } from './components/account/dashboard/dashboard.component';
import { AddresessComponent } from './components/account/addresess/addresess.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { OrderSummaryComponent } from './components/order-summary/order-summary.component';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { ShopComponent } from './components/shop/shop.component';
import { CategoryComponent } from './components/category/category.component';
import { AddresessEditComponent } from './components/account/addresess-edit/addresess-edit.component';
import { AddresessAddComponent } from './components/account/addresess-add/addresess-add.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { FaqComponent } from './components/faq/faq.component';
import { NotFoundComponent } from './components/not-found/not-found.component';


export function tokenGetter() {
  return localStorage.getItem('auth-token')
}


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ItemComponent,
    CartComponent,
    ControlMessagesComponent,
    CheckoutComponent,
    LoginComponent,
    RegisterComponent,
    SafePipe,
    OrderHistoryComponent,
    DashboardComponent,
    OrderSummaryComponent,
    ThankyouComponent,
    ShopComponent,
    CategoryComponent,
    AddresessComponent,
    AddresessEditComponent,
    AddresessAddComponent,
    AboutUsComponent,
    ContactsComponent,
    FaqComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    RxReactiveFormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: [
          'localhost:8000',
          'localhost:4200'
        ],
        disallowedRoutes: [
          'http://localhost:8000/users/register',
          'http://localhost:8000/users/login',
          'http://localhost:8000/categories/all-names',
          // /http:\/\/localhost:8000\/categories\/name\//d,
          'http://localhost:8000/items/all',
          // /http:\/\/localhost:8000\/items\//d,
          // /http:\/\/localhost:8000\/items\/shop-cat\//d,
          // /http:\/\/localhost:8000\/items\/all-by-cat\//d,
          'http://localhost:8000/items/shop',
        ],
      }
    }),
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
