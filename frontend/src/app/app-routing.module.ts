import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { CategoryComponent } from './components/category/category.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ItemComponent } from './components/item/item.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { OrderSummaryComponent } from './components/order-summary/order-summary.component';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { ShopComponent } from './components/shop/shop.component';
import { DashboardComponent } from './components/account/dashboard/dashboard.component';
import { OrderHistoryComponent } from './components/account/order-history/order-history.component';
import { AddresessComponent } from './components/account/addresess/addresess.component';
import { AddresessAddComponent } from './components/account/addresess-add/addresess-add.component';
import { AddresessEditComponent } from './components/account/addresess-edit/addresess-edit.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { FaqComponent } from './components/faq/faq.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthGuardService } from './services/auth-guard.service';



const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'register', component: RegisterComponent
  },
  {
    path: 'item/:item_id', component: ItemComponent
  },
  {
    path: 'order-summary', 
    component: OrderSummaryComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedRoles: ['me']
    }
  },
  {
    path: 'checkout', 
    component: CheckoutComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedRoles: ['me']
    }
  },
  {
    path: 'thankyou', 
    component: ThankyouComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedRoles: ['me']
    }
  },
  {
    path: 'about-us', component: AboutUsComponent,
  },
  {
    path: 'contacts', component: ContactsComponent,
  },
  {
    path: 'faq', component: FaqComponent,
  },
  {
    path: 'shop', component: ShopComponent
  },
  {
    path: 'shop/categories/:cat_id', 
    component: CategoryComponent,
    runGuardsAndResolvers: 'paramsChange'
  },
  {
    path: 'account/dashboard', 
    component: DashboardComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedRoles: ['me']
    }
  },
  {
    path: 'account/orders', 
    component: OrderHistoryComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedRoles: ['me']
    }
  },
  {
    path: 'account/addresses', 
    component: AddresessComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedRoles: ['me']
    }
  },
  {
    path: 'account/addresses/new',
     component: AddresessAddComponent,
     canActivate: [AuthGuardService],
     data: {
      expectedRoles: ['me']
    }
  },
  {
    path: 'account/addresses/:addressId', 
    component: AddresessEditComponent,
    canActivate: [AuthGuardService],
    data: {
      expectedRoles: ['me']
    }
  },
  {
    path: '404', 
    component: NotFoundComponent
  },
  {
    path: '**', 
    redirectTo: '/404'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
