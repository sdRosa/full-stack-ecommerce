
export interface User {
    name: string
    lastname: string
    email: string
    address_id: number
    default_address: boolean
}

export interface UpdateDefaultAddress {
    default_address: boolean
    address_id: number
}