import { Time } from "@angular/common";

export interface userResult {
    id: number
    name: string
    lastname: string
    scopes: string[]
    is_activate: boolean
}

export interface authResult {
    access_token: string;
    token_type: string;
    ttl: number;
}

export interface userRegister {
    name: string;
    lastname?: string;
    email: string;
    password: string;
    password2: string;
}

export interface userRegisterRQ {
    email: string;
    password: string;
    password2: string;
}