export interface CartItem {
    item_id: number
    name: string
    unit_price: number
    quantity: number
    image_url: string
    extraDataDict?: any
}

export interface CartModelServer {
    total: number
    products: CartItem[]
}

export interface DecrementItemCart {
    new_stock: number
    deleted: boolean
}