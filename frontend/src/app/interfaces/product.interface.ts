
export interface ProductPub {
    name: string
    description: string
    availability: boolean
    timestamp: number
    stock: number
    is_active: boolean
    featured: boolean
    image_url: string
    sold: number
    unit_price: number
    discount_price: number
    category_id: number
    id: number
}

export interface ShopPaginated {
    total_items: number
    items: ProductPub[]
    total_pages: number
    current_page: number
}


export interface ProductByCatPriv {
    cat_name: string,
    items: ProductPub[]
}