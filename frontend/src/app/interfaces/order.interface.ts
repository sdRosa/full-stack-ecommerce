import { OrderItem } from "./order-item.interface";


export interface Order {
    payment: number
    subtotal: number
    addressId: number 
    orderItems: [OrderItem]
}

export interface OrderServer {
    id: number
    created_at: number
    payment: number
    subtotal: number
    addressId: number 
    orderItems: [OrderItem] | []
}


export interface BulkCartsModel {
    payment: number 
    address_id: number
    user_id: number
}

export interface SelfOrdersPaginated {
    total_orders: number
    orders: OrderServer[]
    total_pages: number
    current_page: number
}