
export interface OrderItem {
    id: number
    customerId: number 
    itemId: number 
    quantity: number
}