export interface AddressModel {
    user_id: number
    street_address: string
    apartment_address: string
    province: string
    zip: string
    payment_note?: string
}

export interface AddressModelServer {
    id: number
    street_address: string
    apartment_address: string
    province: string
    zip: string
    payment_note: string
}

export interface AddressDelMessage {
    msg: string
    type: string
}