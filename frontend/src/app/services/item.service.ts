import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { ProductPub, ProductByCatPriv, ShopPaginated } from '../interfaces/product.interface';


@Injectable({
  providedIn: 'root'
})
export class ItemService {

  private SERVER_URL = environment.SERVER_URL
  constructor(private http: HttpClient) { }
  
  getAllItems(
    skip: number = 0,
    limit: number = 10,
    is_active: boolean = true,
    ): Observable<ProductPub[]> {
    return this.http.get<ProductPub[]>(this.SERVER_URL + 'items/all', {
      params: {
        skip: skip,
        limit: limit,
        is_active: is_active,
      }
    })
  }


  getAllItemsByCat(
    catId: number,
    skip: number = 0,
    limit: number = 10,
    is_active: boolean = true,
  ): Observable<ProductByCatPriv> {
    return this.http.get<ProductByCatPriv>(this.SERVER_URL + 'items/all-by-cat/' + catId, {
      params : {
        skip: skip,
        limit: limit,
        is_active: is_active,
      }
    })
  }


  getAllSorted(
    skip: number = 0,
    limit: number = 10,
    order_by: string,
    is_active: boolean = true,
  ): Observable<ProductPub[]> {
    return this.http.get<ProductPub[]>(this.SERVER_URL + 'items/all', {
      params: {
        skip: skip,
        limit: limit,
        is_active: is_active,
        order_by: order_by,
      }
    })
  }

  getItemsShopByCat(
    catId: number,
    page: number,
    size: number,
    sort: string
  ): Observable<ShopPaginated> {
    return this.http.get<ShopPaginated>(this.SERVER_URL + 'items/shop-cat/' + catId, {
      params: {
        page: page ? page : 0,
        size: size ? size : 5,
        order_by: sort
      }
    });
  }

  getItemsShop(
    page: number,
    size: number,
    sort: string
  ): Observable<ShopPaginated> {
    return this.http.get<ShopPaginated>(this.SERVER_URL + 'items/shop/', {
      params: {
        page: page ? page : 0,
        size: size ? size : 5,
        order_by: sort
      }
    });
  }

  getItem(itemId: number): Observable<ProductPub> {
    return this.http.get<ProductPub>(this.SERVER_URL + 'items/' + itemId);
  }

}
