import { Injectable } from '@angular/core';

const TOKEN_KEY = 'auth-token'
const USER_KEY = 'auth-user'
const USER_ID_KEY = 'auth-user-id'
const TTL_KEY = 'auth-ttl'

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

  public signOut(): void {
    localStorage.clear();
  }

  public saveToken(token: string): void {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_KEY)!;
  }

  public saveName(name: string): void {
    localStorage.removeItem(USER_KEY);
    localStorage.setItem(USER_KEY, name);
  }

  public getName(): string {
    return localStorage.getItem(USER_KEY)!;
  }

  public saveUserId(name: string): void {
    localStorage.removeItem(USER_ID_KEY);
    localStorage.setItem(USER_ID_KEY, name);
  }

  public getUserId(): string {
    return localStorage.getItem(USER_ID_KEY)!;
  }

  // public saveUser(user): void {
  //   localStorage.removeItem(USER_KEY);
  //   localStorage.setItem(USER_KEY, JSON.stringify(user));
  // }

  // public getUser(): any {
  //   return JSON.parse(localStorage.getItem(USER_KEY)!)
  // }

  public saveTTL(ttl): void {
    localStorage.removeItem(TTL_KEY);
    localStorage.setItem(TTL_KEY, JSON.stringify(ttl));
  }

  public getTTL(): any {
    return JSON.parse(localStorage.getItem(TTL_KEY)!)
  }


}
