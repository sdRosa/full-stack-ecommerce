import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { User, UpdateDefaultAddress } from '../interfaces/user.interface';
import { TokenStorageService } from './token-storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  SERVER_URL = environment.SERVER_URL;

  constructor(private http: HttpClient) { }

  getSelfUser(): Observable<User> {
    return this.http.get<User>(this.SERVER_URL + 'users/self');
  }

  updateDefaultAddress(
    set_default_address: boolean,
    address_id: number
  ): Observable<UpdateDefaultAddress> {
    return this.http.patch<UpdateDefaultAddress>(this.SERVER_URL + 'users/default-address', {}, {
      params: {
        set_default_address: set_default_address,
        address_id: address_id,
      }
    });
  }

}
