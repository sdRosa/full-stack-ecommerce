import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { OrderItem } from '../interfaces/order-item.interface';
import { BulkCartsModel, Order, OrderServer, SelfOrdersPaginated } from '../interfaces/order.interface';
import { Message } from 'src/app/interfaces/message.interface';


@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private orderItems: OrderItem[] = []
  private SERVER_URL = environment.SERVER_URL

  constructor(private http: HttpClient) { }

  getOrder(orderId: number): Observable<Order> {
    return this.http.get<Order>(this.SERVER_URL + 'order/' + orderId);
  }

  getOwnOrders(page: number, size: number): Observable<SelfOrdersPaginated> {
    return this.http.get<SelfOrdersPaginated>(this.SERVER_URL + 'order/self', {
      params: {
        page: page ? page : 0,
        size: size ? size : 3,
      }
    })
  }

  getOrdersSorted(skip: number, limit: number, sort_by: string): Observable<OrderServer[]> {
    return this.http.get<OrderServer[]>(this.SERVER_URL + 'order/all', {
      params: {
        skip: skip ? skip : 0,
        limit: limit ? limit : 3,
        order_by: sort_by ? sort_by : 'created_at',
      }
    });
  }

  createOrder(address_body: BulkCartsModel): Observable<{order_id: number}> {
    return this.http.post<{order_id: number}>(this.SERVER_URL + 'order/bulk-cart', address_body);
  }
}
