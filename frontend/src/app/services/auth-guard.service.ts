import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import decode from 'jwt-decode';
import { InvalidTokenError } from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from './auth.service';

import { TokenStorageService } from './token-storage.service';
import { UserService } from './user.service';


interface TokenPayload {
  sub: number 
  scopes: string[]
}


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private authService: AuthService,
    private tokenService: TokenStorageService,
    private toast: ToastrService,
    private router: Router
  ) { }
  
  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRoles = route.data.expectedRoles;
    const token = this.tokenService.getToken();
    
    try {
      let tokenPayload: TokenPayload = decode(token);
      let checker = (arr, target) => target.every(v => arr.includes(v));
      this.authService.checkAuth();
      if (!this.authService.checkAuth() || !checker(tokenPayload.scopes, expectedRoles)) {
          this.router.navigate(['login']).then(() => {
            this.toast.info(`You should first have to log in`, 'Not Logged', {
              timeOut: 3000,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-bottom-right'
            });
          });
          return false
      }
    
    } catch (e) {
      if (e instanceof InvalidTokenError) {
        this.authService.logged$.next(false)
        return false
      }
    }
    return true 
    
  }
}
