import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { authResult, userRegister } from '../interfaces/auth.interface';
import { TokenStorageService } from './token-storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const httpOptionsForm = {
  headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  logged$ = new BehaviorSubject<boolean>(false) 

  SERVER_URL = environment.SERVER_URL

  constructor(
    private http: HttpClient,
    private tokenService: TokenStorageService,
    private jwtHelper: JwtHelperService,
    private toast: ToastrService,
    private router: Router
  ) { 
    this.checkAuth();
  }

  checkAuth() {
    const token = this.tokenService.getToken();

    if (token) {
      if (this.jwtHelper.isTokenExpired(token)) {
        this.tokenService.signOut()
        this.logged$.next(false);
        return false
      }
      this.logged$.next(true);
      return true
    } else {
      this.logged$.next(false);
      return false
    }
  }

  register(user: userRegister): Observable<any> {
    return this.http.post(this.SERVER_URL + 'users/register', {
      name: user.name,
      lastname: user.lastname,
      email: user.email,
      password: user.password,
      password2: user.password2
    }, httpOptions);
  }

  login(email: string, password: string): Observable<any> {
    const body = new URLSearchParams();
    body.set('username', email);
    body.set('password', password);
    
    return this.http.post(this.SERVER_URL + 'login/access-token', body.toString(), httpOptionsForm);
  }

  logout() {
    this.tokenService.signOut();
    this.logged$.next(false);
    this.router.navigate(['login']);
  }

  // private setSession(authResult: authResult): void {
  //   const expiresAt = moment().add(authResult.expires_in, 'second');

  //   localStorage.setItem('token', authResult.access_token);
  //   localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()) );
  // }
  

  // public isLoggedIn() {
  //   return moment().isBefore(this.getExpiration());
  // }

  // isLoggedOut() {
  //   return !this.isLoggedIn();
  // }

  // getExpiration() {
  //   const expiration = localStorage.getItem("expires_at");
  //   // const expiresAt = expiration !== null ? JSON.parse(expiration) : ;
  //   const expiresAt = JSON.parse(expiration!)
  //   return moment(expiresAt)
  // }


}
