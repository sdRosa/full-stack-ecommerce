import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { CartItem, CartModelServer, DecrementItemCart } from '../interfaces/cart.interface';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class CartService {
  private SERVER_URL = environment.SERVER_URL
  logged!: boolean
  public cartData: CartModelServer = {
    total: 0,
    products: []
  } 

  cartTotal$ = new BehaviorSubject<number>(0)
  cartData$ = new BehaviorSubject<CartModelServer>(this.cartData)

  constructor( 
    private http: HttpClient,
    private authService: AuthService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.authService.checkAuth()
    this.authService.logged$.subscribe(logged => this.logged = logged);

    if (this.logged) {
      this.cartTotal$.next(this.cartData.total);
      this.cartData$.next(this.cartData);

      this.getCartItems().subscribe(
        (cartItems: CartModelServer) => {
          let info = cartItems;

          if (info.products != []) {
            this.cartData = info;
            this.cartTotal$.next(this.cartData.total);
            this.cartData$.next({... this.cartData});
          }
      });
    }
    
  }

  private addItemToRedis(itemId: number): Observable<CartItem> {
    return this.http.post<CartItem>(this.SERVER_URL + 'carts/add/' + itemId, null);
  }

  private decrementItemRedis(itemId: number): Observable<DecrementItemCart> {
    return this.http.patch<DecrementItemCart>(this.SERVER_URL + 'carts/remove-one/' + itemId, null);
  }

  private removeCartItemRedis(item_id: number) {
    return this.http.delete(this.SERVER_URL + 'carts/remove-item/' + item_id);
  }

  private calculateTotal() {
    let Total = 0;

    this.cartData.products.forEach(p => {
      const {quantity} = p;
      const {unit_price} = p;

      Total += quantity * unit_price;
    });
    this.cartData.total = Total;
    this.cartTotal$.next(this.cartData.total);
  }

  public calculateSubtotal(index: number) {
    let unitPrice = this.cartData.products[index].unit_price;
    let quantity = this.cartData.products[index].quantity;

    return unitPrice * quantity
  }


  addItemToCart(itemId: number) {
    this.addItemToRedis(itemId).subscribe(
      (cartItem) => {
        if (this.cartData.products == []) {
          this.cartData.products.push(cartItem);
          // Cart total
          this.calculateTotal();
          this.cartData$.next({... this.cartData});
          // Display toast notification
          this.toast.success(`"${cartItem.name}" added to cart!`, 'Item Added', {
            timeOut: 1500,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-bottom-left'
          });
        } else {
          let index = this.cartData.products.findIndex(p => p.item_id == itemId);
          
          if (index != -1) {
            this.cartData.products[index].quantity = cartItem.quantity;
            // Cart total
            this.calculateTotal();
            // Display toast notification
            this.cartData$.next({... this.cartData});
            this.toast.info(`"${cartItem.name}" added to cart!`, 'Item Quantity Updated', {
              timeOut: 1500,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-bottom-left'
            });
          } else {
            this.cartData.products.push(cartItem);
            this.cartData$.next({... this.cartData});
            // Cart total
            this.calculateTotal();
            // Display toast notification
            this.toast.success(`"${cartItem.name}" added to cart!`, 'Item Added', {
              timeOut: 1500,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-bottom-left'
            });
          }
        }
      },
      (err: HttpErrorResponse) => {
        switch (err.status) {
          case 502:
            this.toast.warning(`Stock empty, can't add to cart`, 'Item Not Added', {
              timeOut: 1500,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-bottom-left'
            })
            break
          case 401:
            this.toast.info(`You should first have to log in`, 'Not Logged', {
              timeOut: 3000,
              progressBar: true,
              progressAnimation: 'increasing',
              positionClass: 'toast-bottom-right'
            });
            break
        }
      }
    )
  }
  

  updateCartItems(index: number, increase: boolean) {
    let data = this.cartData.products[index];
    
    if (increase) {
      this.addItemToRedis(data.item_id).subscribe(
        cartItem => {
          this.cartData.products[index].quantity = cartItem.quantity;
          this.calculateTotal();
          this.cartData$.next({... this.cartData});
        },
        (err: HttpErrorResponse) => {
          switch (err.status) {
            case 404:
              break;
            case 502:
              this.toast.warning(`Stock empty, can't add to cart`, 'Item Not Added', {
                timeOut: 1500,
                progressBar: true,
                progressAnimation: 'increasing',
                positionClass: 'toast-bottom-left'
              });
              break
            case 401:
              this.toast.info(`You should first have to log in`, 'Not Logged', {
                timeOut: 3000,
                progressBar: true,
                progressAnimation: 'increasing',
                positionClass: 'toast-bottom-right'
              });
              break
          }
        }
      )
    } else {
      if (data.quantity == 1) {
        // Delete CartItem
        this.deleteItemCart(index);
      } else {
        data.quantity--;
        this.decrementItemRedis(data.item_id).subscribe(
          (decrementedCartItem: DecrementItemCart) => {
            this.cartData.products[index].quantity = decrementedCartItem.new_stock;
            this.cartData$.next({... this.cartData});
          },
          (err: HttpErrorResponse) => {
          // Toast message
          switch (err.status) {
            case 404:
              break;
            }
          }
        )
      }
    }
  }


  deleteItemCart(index: number) {
    if (window.confirm('Are you sure you want to remove the item?')) {
      let itemToDel = this.cartData.products[index]
      this.removeCartItemRedis(itemToDel.item_id).subscribe(
        data => {
          this.cartData.products.splice(index, 1);
          this.calculateTotal();
        },
        (err: HttpErrorResponse) => {
          switch (err.status) {
            case 404:
              break
          }
        }
      )
      if (this.cartData.total == 0) {
        this.cartData = { total: 0, products: [] };
        this.cartData$.next({... this.cartData});
      } else {
        this.cartData$.next({... this.cartData});
      }

    } else {
      return;
    }
  }


  resetCartData() {
    this.cartData = {
      total: 0,
      products: []
    };
    this.cartData$.next({... this.cartData});
  }

  // RemoveOneCartItem(item_id: number): Observable<CartItemDecremented> {
  //   return this.http.patch<CartItemDecremented>(this.SERVER_URL + 'carts/remove-one', item_id);
  // }

  

  getCartItems(): Observable<CartModelServer> {
    return this.http.get<CartModelServer>(this.SERVER_URL + 'carts/');
  }

  // getCartItemsCount(): Observable<number> {
  //   return this.http.get<number>(this.SERVER_URL + 'carts/count-all');
  // }

}
