import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatNames } from '../interfaces/categories.interface';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  SERVER_URL = environment.SERVER_URL

  constructor(private http: HttpClient) { }

  getCatNames(): Observable<CatNames[]> {
    return this.http.get<CatNames[]>(this.SERVER_URL + 'categories/all-names')
  }

  getCatName(catId: number): Observable<string> {
    return this.http.get<string>(this.SERVER_URL + 'categories/name/' + catId);
  }

}
