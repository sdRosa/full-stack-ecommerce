import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AddressModelServer, 
         AddressModel, 
         AddressDelMessage } from '../interfaces/address.interface';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  SERVER_URL = environment.SERVER_URL;

  constructor(private http: HttpClient) { }

  getAddress(addressId: number): Observable<AddressModelServer> {
    return this.http.get<AddressModelServer>(this.SERVER_URL + 'address/' + addressId);
  }

  getOwnAddresses(skip: number, limit: number): Observable<AddressModelServer[]> {
    return this.http.get<AddressModelServer[]>(this.SERVER_URL + 'address/self', {
      params: {
        skip: skip ? skip : 0,
        limit: limit ? limit : 3,
      }
    });
  }

  getLatestAddress(): Observable<AddressModelServer> {
    return this.http.get<AddressModelServer>(this.SERVER_URL + 'address/latest');
  }

  addAddress(
    set_default: boolean, 
    address_body: AddressModel
  ): Observable<AddressModelServer> {
    return this.http.post<AddressModelServer>(
      this.SERVER_URL + 'address/create', address_body, {
        params: {
          set_default: set_default
        }
    });
  }

  updateAddress(
    addressId: number, 
    updatedAddress: AddressModel
  ): Observable<AddressModel> {
    return this.http.patch<AddressModel>(this.SERVER_URL + 'address/' + addressId, updatedAddress)
  }

  delOwnAddress(addressId: number): Observable<AddressDelMessage> {
    return this.http.delete<AddressDelMessage>(this.SERVER_URL  + 'address/self/' + addressId);
  }
}
